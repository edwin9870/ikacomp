package com.ikacomp.spring;

import com.ikacomp.spring.repository.PlayerRepository;
import com.ikacomp.util.LoggerUtil;
import org.slf4j.Logger;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * Created by Edwin Ramirez on 5/11/2015.
 */
public class MainClass {

  private static Logger logger = LoggerUtil.INSTANCE.getLogger();

  public static void main(String[] args) {
    ApplicationContext context = new ClassPathXmlApplicationContext("repository-context.xml");

    NumericClass numericClass = (NumericClass) context.getBean("numericClass");
    PlayerRepository playerRepository = (PlayerRepository) context.getBean("playerRepository");

    logger.info("Hola mundo, " + numericClass.toString());
    logger.debug("Find player name, " + playerRepository.findByName("lean"));
    logger.debug("Find by player id: " + playerRepository.findByPlayerId(1));
  }
}
