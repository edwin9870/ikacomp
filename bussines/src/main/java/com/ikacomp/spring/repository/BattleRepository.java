package com.ikacomp.spring.repository;

import com.ikacomp.entities.ikariam.board.Battle;
import org.springframework.data.jpa.repository.Temporal;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import javax.persistence.TemporalType;
import java.util.Date;
import java.util.List;

/**
 * Created by Edwin Ramirez on 5/16/2015.
 */

public interface BattleRepository extends CrudRepository<Battle, Integer> {

  List<Battle> findByAllianceWinnerTagAndAllianceLoserTagAndBattleDateIsBetween(@Param("abbreviationWinner") String abbreviationWinner,
                                                                                @Param("abbreviationLoser") String abbreviationLoser,
                                                                                @Param("startDate") @Temporal(TemporalType.DATE) Date startDate,
                                                                                @Param("endDate") @Temporal(TemporalType.DATE) Date endDate);


  List<Battle> findByAllianceTagAndDateIsBetween(@Param("abbreviation") String abbreviation,
                                                 @Param("startDate") @Temporal(TemporalType.DATE) Date startDate,
                                                 @Param("endDate") @Temporal(TemporalType.DATE) Date endDate);

  /**
   * Method for get the count of battle of have a specific threadId
   *
   * @param threadId
   * @return
   */
  Long threadIdCount(@Param("threadId") int threadId);

  /**
   * Method for find all battle who constains the specific status
   * @param status
   * @return
   */
  List<Battle> findByEstatus(@Param("status") String status);

}
