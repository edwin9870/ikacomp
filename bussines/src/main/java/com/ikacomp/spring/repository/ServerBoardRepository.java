package com.ikacomp.spring.repository;

import com.ikacomp.entities.ikariam.board.ServerBoard;
import org.springframework.data.repository.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by Edwin Ramirez on 5/19/2015.
 */
public interface ServerBoardRepository extends Repository<ServerBoard, Integer> {

  @Transactional(readOnly = true)
  List<ServerBoard> findAll();
}
