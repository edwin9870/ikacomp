package com.ikacomp.spring.repository;

import com.ikacomp.entities.ikariam.game.Player;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by Edwin Ramirez on 5/16/2015.
 */

public interface PlayerRepository extends CrudRepository<Player, Integer> {

  @Transactional(readOnly = true)
  Player findByName(@Param("name") String name);

  @Transactional(readOnly = true)
  Player findByPlayerId(@Param("playerId") int playerId);
}
