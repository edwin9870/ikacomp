package com.ikacomp.spring.repository;

import com.ikacomp.entities.ikariam.game.Alliance;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

/**
 * Created by Edwin Ramirez on 5/23/2015.
 * <p>
 * AllianceRepository
 */
public interface AllianceRepository extends CrudRepository<Alliance, Integer> {

  Alliance findByAbbreviation(@Param("abbreviation") String abbreviation);
}
