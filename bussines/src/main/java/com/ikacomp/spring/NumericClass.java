package com.ikacomp.spring;

import javax.inject.Named;
import java.io.Serializable;

/**
 * Created by Edwin Ramirez on 5/11/2015.
 */
@Named
public class NumericClass implements Serializable{
  private static final long serialVersionUID = 763758378377807625L;

  private int firstNumber;
  private int secondNumber;

  public int getFirstNumber() {
    return firstNumber;
  }

  public void setFirstNumber(int firstNumber) {
    this.firstNumber = firstNumber;
  }

  public int getSecondNumber() {
    return secondNumber;
  }

  public void setSecondNumber(int secondNumber) {
    this.secondNumber = secondNumber;
  }

  @Override
  public String toString() {
    return "NumericClass{" +
      "firstNumber=" + firstNumber +
      ", secondNumber=" + secondNumber +
      '}';
  }
}
