package com.ikacomp;

import com.ikacomp.entities.ikariam.game.World;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.PersistenceContext;

/**
 * Created by Edwin Ramirez on 3/1/2015.
 */
public class SampleFinding {

    private EntityManager entityManager;

    public String getSample() {

        World world = Persistence.createEntityManagerFactory("ika-comp").createEntityManager().find(World.class, 1);
        return world.toString();
    }
}
