package com.ikacomp;

import com.ikacomp.entities.ikariam.board.Battle;
import com.ikacomp.util.LoggerUtil;
import org.slf4j.Logger;

import javax.persistence.CacheStoreMode;
import javax.persistence.EntityManager;
import javax.persistence.Persistence;
import javax.persistence.TemporalType;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

//import org.slf4j.Logger;

/**
 * Created by Edwin Ramirez on 4/13/2015.
 */
public class BattleDAO {

  private EntityManager entityManager;
  private Logger logger = LoggerUtil.INSTANCE.getLogger();

  public Map<String, List<Battle>> findBattleByStartDateEndDateAllianceTags(String firstAllianceAbbreviation,
                                                                            String secondAllianceAbbreviation, Date startDate,
                                                                            Date endDate) {
    logger.info("Start to execute findBattleByStartDateEndDateAllianceTags");
    entityManager = Persistence.createEntityManagerFactory("ika-comp").createEntityManager();

    List<Battle> battlesWinner = entityManager.createNamedQuery("Battle.findByAllianceWinnerTagAndAllianceLoserTagAndBattleDateIsBetween")
      .setParameter("abbreviationWinner", firstAllianceAbbreviation)
      .setParameter("abbreviationLoser", secondAllianceAbbreviation)
      .setParameter("startDate", startDate)
      .setParameter("endDate", endDate).getResultList();

    HashMap<String, List<Battle>> battles = new HashMap<>();
    battles.put("FIRST_ALLIANCE_BATTLES", battlesWinner);

    logger.info("Battle winner: " + battlesWinner);

    logger.info("Finish execute findBattleByStartDateEndDateAllianceTags");
    return battles;
  }

  public List<Battle> findAllBatlesByAllianceAbbreviationAndDates(String allianceAbbreviation, Date startDate, Date endDate) {
    logger.info("Start to execute findAllBatlesByAlliance");
    entityManager = Persistence.createEntityManagerFactory("ika-comp").createEntityManager();
    List allianceBattle = entityManager.createNamedQuery("Battle.findByAllianceTagAndDateIsBetween")
      .setParameter("abbreviation", allianceAbbreviation)
      .setParameter("startDate", startDate, TemporalType.DATE)
      .setParameter("endDate", endDate, TemporalType.DATE)
      .setHint("javax.persistence.cache.storeMode", CacheStoreMode.REFRESH)
      .getResultList();

    logger.info("Finish to execute findAllBatlesByAlliance");
    return allianceBattle;
  }

}
