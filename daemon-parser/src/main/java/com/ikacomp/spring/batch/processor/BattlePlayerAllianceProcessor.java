package com.ikacomp.spring.batch.processor;

import com.ikacomp.Constant;
import com.ikacomp.battles.AllianceWinner;
import com.ikacomp.entities.ikariam.board.Battle;
import com.ikacomp.entities.ikariam.game.Alliance;
import com.ikacomp.quartz.AllianceLoser;
import com.ikacomp.util.LoggerUtil;
import com.ikacomp.util.sdo.BattleDocument;
import org.slf4j.Logger;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Set;

/**
 * Created by Edwin Ramirez on 5/23/2015.
 * <p>
 * Class used for process defenders player
 */
public class BattlePlayerAllianceProcessor implements ItemProcessor<BattleDocument, Battle> {

  private Logger logger = LoggerUtil.INSTANCE.getLogger();

  @Autowired
  private AllianceWinner allianceWinner;
  @Autowired
  private AllianceLoser allianceLoser;

  @Override
  public Battle process(BattleDocument battleDocument) throws Exception {
    logger.info("BattlePlayerAllianceProcessor start to execute");

    if (battleDocument.getBattle().getStatus().equals(Constant.Status.NP.getAbbreviacion())) {
      logger.info("Battle filter because have status NP ({})", Constant.Status.NP.getDescripcion());
      return battleDocument.getBattle();
    }

    Battle battle = battleDocument.getBattle();
    Set<Alliance> winnerAlliances = allianceWinner.getWinnerAlliances(battle, battle.getWinners());

    logger.debug("Parameters to get allianceLoser. Battle: {}, aattackers: {}, defenders: {} and winners: {}", battle, battle.getAttackers(), battle.getDefenders(), battle.getWinners());
    Set<Alliance> loserAlliances = allianceLoser.getLoserAlliances(battle, battle.getAttackers(), battle.getDefenders(), battle.getWinners());

    battle.setAlliancesWinner(winnerAlliances);
    battle.setAlliancesLoser(loserAlliances);

    battle.setStatus(Constant.Status.OK.getAbbreviacion());

    return battle;
  }

}
