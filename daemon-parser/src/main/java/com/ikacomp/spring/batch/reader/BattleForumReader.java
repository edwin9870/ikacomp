package com.ikacomp.spring.batch.reader;

import com.ikacomp.Constant;
import com.ikacomp.entities.ikariam.board.Battle;
import com.ikacomp.entities.ikariam.board.ServerBoard;
import com.ikacomp.parser.interfaces.BattleDocument;
import com.ikacomp.spring.repository.BattleRepository;
import com.ikacomp.spring.repository.ServerBoardRepository;
import com.ikacomp.util.LoggerUtil;
import org.apache.http.NameValuePair;
import org.apache.http.client.utils.URLEncodedUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.slf4j.Logger;
import org.springframework.batch.item.ItemReader;
import org.springframework.batch.item.NonTransientResourceException;
import org.springframework.batch.item.ParseException;
import org.springframework.batch.item.UnexpectedInputException;
import org.springframework.beans.factory.BeanInitializationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.net.URI;
import java.util.List;
import java.util.ListIterator;

import static com.ikacomp.Constant.BattleLimit.BATTLE_LIMIT;

/**
 * Created by Edwin Ramirez on 5/19/2015.
 * <p>
 * Class for reading each battle posted
 */
@Service
public class BattleForumReader implements ItemReader<Battle> {


  short cantidadBatallasExistente;
  private ListIterator<ServerBoard> servers;
  private Document document;
  private ListIterator<Battle> battles;
  private ServerBoard server;
  private int i;
  private Logger logger = LoggerUtil.INSTANCE.getLogger();

  @Autowired
  private ServerBoardRepository serverBoardRepository;
  @Autowired
  private BattleDocument battleDocument;
  @Autowired
  private BattleRepository battleRepository;


  @Override
  public Battle read() throws UnexpectedInputException, ParseException, NonTransientResourceException {
    logger.info("Start method BattleForumReader");
    if (!battles.hasNext()) {
      processNextPageBattles();
    }

    try {

      Battle battle = battles.next();

      int threadID;
      List<NameValuePair> params = URLEncodedUtils.parse(new URI(battle.getUrl()), "UTF-8");

      threadID = params.stream().filter(param -> param.getName().equals(Constant.QueryString.THREAD_ID.getKeyName()))
        .mapToInt(param -> Integer.parseInt(param.getValue())).findFirst().getAsInt();

      logger.debug("battleRepository.threadIdCount(threadID): {}", battleRepository.threadIdCount(threadID).toString());
      boolean existeBatalla = (battleRepository.threadIdCount(threadID) > 0);
      if (existeBatalla) {
        cantidadBatallasExistente++;
        logger.info("Battle found. Count battle that has been found: " + cantidadBatallasExistente);

        if (cantidadBatallasExistente == BATTLE_LIMIT.getBattleLimit()) {
          logger.info("Finish to fetching battles");
          return null;
        }

        return new Battle();
      }
      battle.setThreadId(threadID);
      battle.setStatus(Constant.Status.AC.getAbbreviacion());
      battle.setServerBoard(server);
      logger.info("battle returned");

      return battle;
    } catch (Exception e) {
      logger.error("Error", e);
      return null;
    }
  }

  /**
   * Spring initialize method
   */
  public void init() {
    i = 1;
    cantidadBatallasExistente = 0;
    servers = serverBoardRepository.findAll().listIterator();
    logger.info("Server list: {}", servers.toString());

    if (servers.hasNext()) {
      server = servers.next();
    }

    processNextPageBattles();

  }

  private void processNextPageBattles() {
    String url = server.getUrl();

    try {

      url = formatUrlWithIndex(url, i++);
      logger.info("processNextPageBattles executed, url: {}", url);
      document = Jsoup.connect(url).timeout(Constant.HttpTimeOut.TIME_OUT.getTimeOut()).get();
      battles = battleDocument.getBattles(document).listIterator();
    } catch (IOException e) {
      logger.error("IO error", e);
      throw new BeanInitializationException(e.getMessage(), e.getCause());
    }
  }

  //TODO: Move to a utility Class
  private String formatUrlWithIndex(String url, int index) {
    url = url.substring(0, url.lastIndexOf("=") + 1);
    url = url.concat(Integer.toString(index));
    return url;
  }


}
