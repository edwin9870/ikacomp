package com.ikacomp.spring.batch.writer;

import com.ikacomp.entities.ikariam.board.Battle;
import com.ikacomp.spring.repository.BattleRepository;
import com.ikacomp.util.LoggerUtil;
import org.slf4j.Logger;
import org.springframework.batch.item.ItemWriter;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

/**
 * Created by Edwin Ramirez on 5/17/2015.
 */
public class BattleForumWriter implements ItemWriter<Battle> {
  private Logger logger = LoggerUtil.INSTANCE.getLogger();

  @Autowired
  private BattleRepository battleRepository;


  @Override
  public void write(List<? extends Battle> list) throws Exception {
    logger.info("BattleForumWriter executed");
    
    logger.info("Battles to save: {}", list);
    battleRepository.save(list);

    logger.info("Battles saved!");
  }
}
