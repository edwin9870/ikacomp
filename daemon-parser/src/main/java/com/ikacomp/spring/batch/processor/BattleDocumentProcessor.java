package com.ikacomp.spring.batch.processor;

import com.ikacomp.Constant;
import com.ikacomp.entities.ikariam.board.Battle;
import com.ikacomp.parser.impl.ReportParserFactory;
import com.ikacomp.parser.interfaces.ParticipantBattle;
import com.ikacomp.util.LoggerUtil;
import com.ikacomp.util.sdo.BattleDocument;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.slf4j.Logger;
import org.springframework.batch.item.ItemProcessor;

/**
 * Created by Edwin Ramirez on 5/23/2015.
 * <p>
 * TODO: Changed description if i will used this class
 * Processor class used to find Battle Document
 */
public class BattleDocumentProcessor implements ItemProcessor<Battle, BattleDocument> {

  private Logger logger = LoggerUtil.INSTANCE.getLogger();

  @Override
  public BattleDocument process(Battle battle) throws Exception {
    logger.info("BattleDocumentProcessor start to execute");

    String url = battle.getUrl();

    Document document = Jsoup.connect(url)
      .userAgent(Constant.UserAgent.FIREFOX.getUserAgent())
      .timeout(Constant.HttpTimeOut.TIME_OUT.getTimeOut())
      .get();

    ParticipantBattle participantBattle = ReportParserFactory.getInstance(document);

    if (participantBattle == null) {
      battle.setStatus(com.ikacomp.Constant.Status.NP.getAbbreviacion());
    }

    return new BattleDocument(battle, document);
  }
}
