package com.ikacomp.spring.batch.writer;

import com.ikacomp.Constant;
import com.ikacomp.entities.ikariam.board.Battle;
import com.ikacomp.entities.ikariam.game.Alliance;
import com.ikacomp.entities.ikariam.game.Player;
import com.ikacomp.spring.repository.AllianceRepository;
import com.ikacomp.spring.repository.BattleRepository;
import com.ikacomp.spring.repository.PlayerRepository;
import com.ikacomp.util.LoggerUtil;
import org.slf4j.Logger;
import org.springframework.batch.item.ItemWriter;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Created by Edwin Ramirez on 5/24/2015.
 * <p>
 * Writer class for persist battles
 */
public class BattleWriter implements ItemWriter<Battle> {

  private Logger logger = LoggerUtil.INSTANCE.getLogger();

  @Autowired
  private BattleRepository battleRepository;
  @Autowired
  private AllianceRepository allianceRepository;
  @Autowired
  private PlayerRepository playerRepository;


  @Override
  public void write(List<? extends Battle> battles) throws Exception {
    logger.info("Start to executed write. List: {}", battles.toString());

    for (Battle battle : battles) {
      if (battle.getStatus().equals(Constant.Status.NP.getAbbreviacion())) {
        logger.info("Battle in status NP");
        battleRepository.save(battle);
        continue;
      }

      try {

        battle.setAttackers(persistPlayers(battle.getAttackers()));
        battle.setDefenders(persistPlayers(battle.getDefenders()));
        battle.setWinners(persistPlayers(battle.getWinners()));

        battle.setAlliancesWinner(persistAlliance(battle.getAlliancesWinner()));
        battle.setAlliancesLoser(persistAlliance(battle.getAlliancesLoser()));


        logger.info("Battle {} saved!", battle);

        battleRepository.save(battle);
      } catch (Exception e) {
        logger.info("Error try to persit the battle. Saved the battle in status RE");
        setBattleRe(battle);

        battleRepository.save(battle);

      }
    }
    logger.info("Battles saved!");
  }

  private void setBattleRe(Battle battle) {
    battle.setAttackers(null);
    battle.setDefenders(null);
    battle.setWinners(null);
    battle.setAlliancesWinner(null);
    battle.setAlliancesLoser(null);
    battle.setStatus(Constant.Status.RE.getAbbreviacion());
  }

  /**
   * Method for persist alliances who don't exits
   *
   * @param alliances
   */
  private Set<Alliance> persistAlliance(Set<Alliance> alliances) {
    alliances = alliances.stream()
      .filter(alliance -> alliance.getAbbreviation() != null)
      .map(alliance -> {
        Alliance allianceEntity = allianceRepository.findByAbbreviation(alliance.getAbbreviation());
        logger.info("allianceAbreviation: {}", alliance.getAbbreviation());
        logger.info("AllianceEntity: {}", allianceEntity);


        if (allianceEntity == null) {
          logger.info("Alliance: {}, persisted", alliance.getAbbreviation());
          allianceEntity = allianceRepository.save(alliance);
        }

        return allianceEntity;
      }).collect(Collectors.toSet());

    logger.info("Alliances returned: {}", alliances);

    return alliances;
  }

  /**
   * Method for persist player who don't exits
   *
   * @param players
   */
  private Set<Player> persistPlayers(Set<Player> players) {
    players = players.stream().map(player -> {
      Player playerEntity = playerRepository.findByName(player.getName());
      logger.info("PlayerEntity: {}", playerEntity);
      if (playerEntity == null) {
        logger.info("Player: {}, persisted", player.getName());
        playerEntity = playerRepository.save(player);
      }

      logger.info("playerEntity returned: {}", playerEntity);
      return playerEntity;
    }).collect(Collectors.toSet());

    logger.info("Players returned: {}", players);

    return players;
  }
}
