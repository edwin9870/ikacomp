package com.ikacomp.spring.batch.processor;

import com.ikacomp.Constant;
import com.ikacomp.entities.ikariam.game.Player;
import com.ikacomp.parser.impl.ReportParserFactory;
import com.ikacomp.parser.interfaces.ParticipantBattle;
import com.ikacomp.util.LoggerUtil;
import com.ikacomp.util.sdo.BattleDocument;
import org.slf4j.Logger;
import org.springframework.batch.item.ItemProcessor;

import java.util.Set;

/**
 * Created by Edwin Ramirez on 5/23/2015.
 * <p>
 * Class used for process Players (players, winner and defenders)
 */
public class BattlePlayerProcessor implements ItemProcessor<BattleDocument, BattleDocument> {

  private Logger logger = LoggerUtil.INSTANCE.getLogger();

  @Override
  public BattleDocument process(BattleDocument battleDocument) throws Exception {
    logger.info("BattlePlayerProcessor start to execute");
    if (battleDocument.getBattle().getStatus().equals(Constant.Status.NP.getAbbreviacion())) {
      logger.info("Battle filter because have status NP ({})", Constant.Status.NP.getDescripcion());
      return battleDocument;
    }

    ParticipantBattle participantBattle = ReportParserFactory.getInstance(battleDocument.getDocument());
    Set<Player> attackers = participantBattle.getAttackers(battleDocument.getDocument());
    Set<Player> defenders = participantBattle.getDefenders(battleDocument.getDocument());
    Set<Player> winners = participantBattle.getWinners(battleDocument.getDocument());

    if (attackers.isEmpty()) {
      logger.info("Battle filter because not attacker have been found");
      battleDocument.getBattle().setStatus(Constant.Status.NP.getAbbreviacion());
      return battleDocument;
    }


    battleDocument.getBattle().setAttackers(attackers);
    battleDocument.getBattle().setDefenders(defenders);
    battleDocument.getBattle().setWinners(winners);

    return battleDocument;
  }


}