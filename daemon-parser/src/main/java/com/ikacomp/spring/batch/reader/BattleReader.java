package com.ikacomp.spring.batch.reader;

import com.ikacomp.Constant;
import com.ikacomp.entities.ikariam.board.Battle;
import com.ikacomp.spring.repository.BattleRepository;
import com.ikacomp.util.LoggerUtil;
import org.slf4j.Logger;
import org.springframework.batch.item.ItemReader;
import org.springframework.batch.item.NonTransientResourceException;
import org.springframework.batch.item.ParseException;
import org.springframework.batch.item.UnexpectedInputException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.ListIterator;

/**
 * Created by Edwin Ramirez on 5/23/2015.
 *
 * Class used for reading the battles in status AC and returned it for processing
 */
@Service
public class BattleReader implements ItemReader<Battle> {

  ListIterator<Battle> battles;
  private Logger logger = LoggerUtil.INSTANCE.getLogger();
  @Autowired
  private BattleRepository battleRepository;

  /**
   * Inicializer method
   */
  public void init() {
    List<Battle> battlesFounded = battleRepository.findByEstatus(Constant.Status.AC.getAbbreviacion());
    battles = battlesFounded.listIterator();
    logger.info("List of battle to process: {}", battlesFounded.size());
  }

  @Override
  public Battle read() throws UnexpectedInputException, ParseException, NonTransientResourceException {
    if (!battles.hasNext()) {
      logger.info("No more battle to process, reading method finalized");
      return null;
    }

    logger.info("Battle returned for processing");
    return battles.next();
  }
}
