package com.ikacomp.spring.batch.processor;

import com.ikacomp.entities.ikariam.board.Battle;
import com.ikacomp.util.LoggerUtil;
import org.slf4j.Logger;
import org.springframework.batch.item.ItemProcessor;

/**
 * Created by Edwin Ramirez on 5/23/2015.
 *
 * Processor class used for filter battle that has empty values
 */
public class BattleForumValidator implements ItemProcessor<Battle, Battle> {
  private Logger logger = LoggerUtil.INSTANCE.getLogger();

  @Override
  public Battle process(Battle battle) throws Exception {
    logger.info("BattleForumValidator executed.");

    if(battle.isEmpty()) {
      logger.info("Battle filtered");
      return null;
    }

    logger.info("Battle: {}", battle.toString());
    return battle;
  }
}
