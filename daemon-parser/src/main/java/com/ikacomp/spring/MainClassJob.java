package com.ikacomp.spring;

import com.ikacomp.util.LoggerUtil;
import org.slf4j.Logger;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.JobParameters;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * Created by Edwin Ramirez on 5/17/2015.
 */
public class MainClassJob {

  private static Logger logger = LoggerUtil.INSTANCE.getLogger();

  /**
   * Class used for execute the processing battle job
   *
   */
  public static void main(String [] args) {

    String[] springConfig = {"job-context.xml", "repository-context.xml", "parser-context.xml"};

    ApplicationContext context = new ClassPathXmlApplicationContext(springConfig);

    JobLauncher jobLauncher = (JobLauncher) context.getBean("jobLauncher");
    Job job = (Job) context.getBean("battleProcessing");

    try {
      JobExecution execution = jobLauncher.run(job, new JobParameters());
      logger.info("Job status: {}", execution.getStatus());
    } catch (Exception e) {
      logger.info("Error executing job", e);
    }
  }
}
