package com.ikacomp.dao;


import com.ikacomp.entities.ikariam.game.Alliance;
import com.ikacomp.util.EntityManagerUtil;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import java.util.Set;

import static java.util.stream.Collectors.toSet;

public class AllianceServiceImpl implements AllianceService {


  private final EntityManager entityManager;

  public AllianceServiceImpl() {
    entityManager = EntityManagerUtil.INSTANCE.getInstance();
  }

  @Override
  public Set<Alliance> persistAlliances(Set<Alliance> alliances) {
    if (!entityManager.getTransaction().isActive()) {
      entityManager.getTransaction().begin();
    }
    alliances = alliances.stream()
      .filter(alliance -> alliance.getAbbreviation() != null)
      .map(alliance -> {
          TypedQuery<Alliance> allianceEntity = entityManager
            .createNamedQuery("Alliance.findByAbbreviation", Alliance.class)
            .setParameter("abbreviation", alliance.getAbbreviation());
          if (allianceEntity.getResultList().size() < 1) {
            entityManager.persist(alliance);
            allianceEntity = entityManager
            .createNamedQuery("Alliance.findByAbbreviation", Alliance.class)
            .setParameter("abbreviation", alliance.getAbbreviation());
          }
          return allianceEntity.getSingleResult();
        })
      .collect(toSet());
    return alliances;
  }
}