package com.ikacomp.dao;

import com.ikacomp.entities.ikariam.game.Alliance;

import java.util.Set;

/**
 * Created by Edwin Ramirez on 5/4/2015.
 */
public interface AllianceService {

  /**
   * Method for persit a alliance list
   *
   * @param alliances
   * @return
   */
  Set<Alliance> persistAlliances(Set<Alliance> alliances);
}
