package com.ikacomp.quartz;

import com.ikacomp.util.LoggerUtil;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.slf4j.Logger;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.JobParameters;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * Created by yoya on 2/27/15.
 */
public class ProcessBattleJob implements Job {

  private Logger logger = LoggerUtil.INSTANCE.getLogger();

  @Override
  public void execute(JobExecutionContext context) throws JobExecutionException {

    String[] springConfig = {"job-context.xml", "repository-context.xml", "parser-context.xml"};

    ApplicationContext classPathXmlApplicationContext = new ClassPathXmlApplicationContext(springConfig);

    JobLauncher jobLauncher = (JobLauncher) classPathXmlApplicationContext.getBean("jobLauncher");
    org.springframework.batch.core.Job job = (org.springframework.batch.core.Job) classPathXmlApplicationContext.getBean("battleProcessing");

    try {
      JobExecution execution = jobLauncher.run(job, new JobParameters());
      logger.info("Job status: {}", execution.getStatus());
    } catch (Exception e) {
      logger.info("Error executing job", e);
    }
  }
}
