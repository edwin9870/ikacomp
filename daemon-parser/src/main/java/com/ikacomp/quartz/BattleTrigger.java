package com.ikacomp.quartz;

import com.ikacomp.util.LoggerUtil;
import org.quartz.*;
import org.quartz.impl.StdSchedulerFactory;
import org.slf4j.Logger;

/**
 * Created by yoya on 2/27/15.
 *
 * Class used for execute the Job every 1 hour
 */
public class BattleTrigger {

  private static Logger logger = LoggerUtil.INSTANCE.getLogger();

  public void execute() throws SchedulerException {
    JobDetail job = JobBuilder.newJob(ProcessBattleJob.class).withIdentity("processBattleJob", "battleTrigger").build();

    Trigger trigger = TriggerBuilder.newTrigger()
      .withIdentity("processBattleJob", "battleTrigger")
      .withSchedule(CronScheduleBuilder.cronSchedule("0 0/1 * * * ?"))
      .build();

    Scheduler scheduler = new StdSchedulerFactory().getScheduler();
    scheduler.start();
    scheduler.scheduleJob(job, trigger);
  }

  public static void main(String[] args) throws SchedulerException {
    logger.info("Start to execute scheulder");
    new BattleTrigger().execute();
  }
}
