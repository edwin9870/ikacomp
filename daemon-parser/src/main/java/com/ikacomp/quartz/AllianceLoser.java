package com.ikacomp.quartz;

import com.ikacomp.entities.ikariam.board.Battle;
import com.ikacomp.entities.ikariam.game.Alliance;
import com.ikacomp.entities.ikariam.game.Player;
import org.springframework.stereotype.Service;

import java.util.Set;

/**
 * Created by Edwin Ramirez on 5/4/2015.
 */
@Service
public interface AllianceLoser {


  /**
   * Method for getting the loser alliances of a battle
   *
   * @param battle
   * @param attackers
   * @param defenders
   * @param winners
   * @return
   */
  Set<Alliance> getLoserAlliances(Battle battle, Set<Player> attackers, Set<Player> defenders, Set<Player> winners);
}
