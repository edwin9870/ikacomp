package com.ikacomp.battles;

import com.ikacomp.entities.ikariam.board.Battle;
import com.ikacomp.entities.ikariam.game.Alliance;
import com.ikacomp.entities.ikariam.game.Player;
import org.springframework.stereotype.Service;

import java.util.Set;

/**
 * Created by Edwin Ramirez on 5/4/2015.
 */
@Service
public interface AllianceWinner {

  /**
   * * Method for getting the Winner alliances of a battle
   *
   * @param battle
   * @param winners
   * @return
   */
  Set<Alliance> getWinnerAlliances(Battle battle, java.util.Set<Player> winners);
}
