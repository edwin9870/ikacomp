package com.ikacomp.battles;

import com.ikacomp.entities.ikariam.board.Battle;
import com.ikacomp.entities.ikariam.game.Alliance;
import com.ikacomp.entities.ikariam.game.Player;
import org.springframework.stereotype.Component;

import java.util.HashSet;
import java.util.Set;

@Component
public class AllianceWinnerImpl implements AllianceWinner {
  public AllianceWinnerImpl() {
  }

  /**
   * * Method for getting the Winner alliances of a battle
   *
   * @param battle
   * @param winners
   * @return
   */
  @Override
  public Set<Alliance> getWinnerAlliances(Battle battle, java.util.Set<Player> winners) {
    Set<Alliance> winner_alliances = new HashSet<Alliance>();

    winners.stream().filter(player -> player.getAlliance() != null).forEach(player -> {
        player.getAlliance().setServerBoard(battle.getServerBoard());
        winner_alliances.add(player.getAlliance());
      });
    return winner_alliances;
  }
}