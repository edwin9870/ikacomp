package com.ikacomp.battles;

import com.ikacomp.entities.ikariam.board.*;
import com.ikacomp.entities.ikariam.game.*;
import com.ikacomp.quartz.*;
import org.springframework.stereotype.Component;

import java.util.HashSet;
import java.util.Set;

@Component
public class AllianceLoserimpl implements AllianceLoser {


  public AllianceLoserimpl() {
  }

  @Override
  public Set<Alliance> getLoserAlliances(Battle battle, Set<Player> attackers, Set<Player> defenders, Set<Player> winners) {
    Set<Alliance> loser_alliances = new HashSet<Alliance>();

    if (!winners.contains(attackers.iterator().next())) {
      attackers.stream().filter(player -> player.getAlliance() != null).forEach(player -> {
          player.getAlliance().setServerBoard(battle.getServerBoard());
          loser_alliances.add(player.getAlliance());
      });
    } else {
      defenders.stream().filter(player -> player.getAlliance() != null).forEach(player -> {
          player.getAlliance().setServerBoard(battle.getServerBoard());
          loser_alliances.add(player.getAlliance());
        });
    }
    return loser_alliances;
  }
}