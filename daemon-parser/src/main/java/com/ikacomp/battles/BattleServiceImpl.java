package com.ikacomp.battles;

import com.ikacomp.entities.ikariam.board.Battle;
import com.ikacomp.util.EntityManagerUtil;
import com.ikacomp.util.LoggerUtil;
import org.slf4j.Logger;

import javax.persistence.EntityManager;

public class BattleServiceImpl implements BattleService {

  private EntityManager entityManager;
  private Logger logger = LoggerUtil.INSTANCE.getLogger();

  public BattleServiceImpl() {
    entityManager = EntityManagerUtil.INSTANCE.getInstance();
  }

  @Override
  public boolean persitBattle(Battle battle) {
    logger.info("Start to persit battle: {}", battle);

    entityManager.getTransaction().begin();
    entityManager.persist(battle);
    entityManager.getTransaction().commit();
    logger.info("battle persited");

    return true;
  }
}