package com.ikacomp.battles;

import com.ikacomp.entities.ikariam.board.Battle;

/**
 * Created by Edwin Ramirez on 5/4/2015.
 */
public interface BattleService {

  /**
   * Method for persit a battle to the database
   * @param battle
   */
  boolean persitBattle(Battle battle);
}
