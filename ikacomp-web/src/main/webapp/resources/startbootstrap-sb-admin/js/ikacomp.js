var dataTable;
const WIN_BATTLE = "ganada";
var lastHighNumber = 0;

$( document ).ready(function() {

    $('.input-daterange input').datepicker({
        language: 'es'
    });

    dataTable = $('.result-panel #result-table').DataTable({
        "columnDefs": [ {
                "targets": 0,
                "searchable": false,
                "orderable": false,
                "defaultContent": ""
            },
            {
                "targets": 1,
                "data": "title",
                "render": getTitle

            },
            {
                "targets": 2,
                "data": "win",

                "render": getBattleStatus
            }
        ],
        "order": [[ 1, 'asc' ]]
    });

    dataTable.on( 'order.dt search.dt', function () {

        cleanBattleSummary();

        dataTable.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
            cell.innerHTML = i+1;
        });

        dataTable.column(2, {search:'applied', order:'applied'}).nodes().each(processBattle);

    } ).draw();
});


$('form input[type="button"]').click(function(){
    var form = $('form');
    var winnerAllianceTag = form.find('input[name=winner-alliance]').val();
    var loserAllianceTag = form.find('input[name=loser-alliance]').val();
    var startDate = form.find('input[name=start-date]').val();
    var endDate = form.find('input[name=end-date]').val();
    var recaptcha = form.find('textarea[name=g-recaptcha-response]').val();

    var url = "/battles/"+winnerAllianceTag+"/"+loserAllianceTag+"/"+startDate+"/"+endDate+"/"+recaptcha;

    dataTable.ajax.url(url).load();

});

function getTitle( data, type, allData, meta) {
    return '<a href="'+allData.url+'" target="_blank" class="text-info">'+allData.title+'</a>';
}

function getBattleStatus( data, type, allData, meta) {

    return data == true ? '<span class="result text-success"><i class="fa fa-trophy" title="Ganada"><span class="hidden">Ganada</span></i></span>' : '<span class="result text-danger"><i class="fa fa-times" title="Perdida"><span class="hidden">Perdida</span></i></span>';
}

function cleanBattleSummary() {
    $('span[data-resumen-batalla="win"]').text(0);
    $('span[data-resumen-batalla="lose"]').text(0);
    lastHighNumber = 0;
}

function processBattle(cell, i) {
    var result = $(cell).text().toLowerCase();

    if(lastHighNumber > i) {
        return;
    }

    lastHighNumber = i;

    if(result == WIN_BATTLE) {
        var countBattlesWin  = $('span[data-resumen-batalla="win"]').text();
        $('span[data-resumen-batalla="win"]').text(parseInt(countBattlesWin)+1);
    } else {
        var countbattlesLose = $('span[data-resumen-batalla="lose"]').text();
        $('span[data-resumen-batalla="lose"]').text(parseInt(countbattlesLose)+1);
    }
}