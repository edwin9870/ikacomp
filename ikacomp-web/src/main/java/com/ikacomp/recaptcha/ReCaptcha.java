package com.ikacomp.recaptcha;

import com.ikacomp.Constant;
import com.ikacomp.util.LoggerUtil;
import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.io.IOException;

/**
 * Created by Edwin Ramirez on 6/6/2015.
 * Class for validate ReChapta
 */
@Component
public class ReCaptcha {

  @Value("6LcB-QcTAAAAAB5CDOft8Fo9HmqFdgQBsYPT2gkl")
  private String secretKey;
  private String gCaptcha;
  private String remoteIp;

  private Logger logger = LoggerUtil.INSTANCE.getLogger();


  /**
   * Method for validate the recaptcha
   *
   * @return true if is valid or false is not valid
   */
  public boolean isValid() {
    try {
      HttpResponse<JsonNode> jsonResponse = Unirest.post(Constant.URL.GOOGLE_RECAPTCHA.getUrl())
        .field("secret", secretKey)
        .field("response", gCaptcha)
        .field("remoteip", remoteIp).asJson();

      Unirest.shutdown();

      boolean success = jsonResponse.getBody().getObject().getBoolean("success");

      logger.info("Success: {}", success);

      return success;
    } catch (UnirestException | IOException e) {
      logger.error("Error consultando", e);
    }

    return false;
  }

  public String getSecretKey() {
    return secretKey;
  }

  public void setSecretKey(String secretKey) {
    this.secretKey = secretKey;
  }

  public String getgCaptcha() {
    return gCaptcha;
  }

  public void setgCaptcha(String gCaptcha) {
    this.gCaptcha = gCaptcha;
  }

  public String getRemoteIp() {
    return remoteIp;
  }

  public void setRemoteIp(String remoteIp) {
    this.remoteIp = remoteIp;
  }
}
