package com.ikacomp.web.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * Created by Edwin Ramirez on 5/25/2015.
 */
@Controller
@RequestMapping("/sample")
public class Sample {

  @RequestMapping(value = "/{mensaje}", method = RequestMethod.GET)
  public String getMovie(@PathVariable String mensaje, ModelMap model) {
    model.addAttribute("name", "Edwiiiiin");
    model.addAttribute("mensaje", mensaje);
    return "hola";
  }
}
