package com.ikacomp.web.controller;

import com.ikacomp.interceptor.ReCaptchaInterceptor;
import com.ikacomp.util.LoggerUtil;
import org.slf4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpSession;

/**
 * Created by Edwin Ramirez on 5/26/2015.
 */
@Controller
@RequestMapping("/")
public class MainPage {

  private Logger logger = LoggerUtil.INSTANCE.getLogger();

  @RequestMapping(value = {"/", "/index"}, method = RequestMethod.GET)
  public String getIndex(HttpSession httpSession, final Model model) {

    model.addAttribute("isCaptchaValid", httpSession.getAttribute(ReCaptchaInterceptor.CAPTCHA_VALID_KEY));
    return "index";
  }

}
