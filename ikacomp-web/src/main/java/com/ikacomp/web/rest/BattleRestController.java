package com.ikacomp.web.rest;

import com.ikacomp.Constant;
import com.ikacomp.dto.AllianceBattleDTO;
import com.ikacomp.entities.ikariam.board.Battle;
import com.ikacomp.spring.repository.BattleRepository;
import com.ikacomp.util.LoggerUtil;
import org.dozer.Mapper;
import org.slf4j.Logger;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.inject.Inject;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Created by Edwin Ramirez on 5/30/2015.
 * <p>
 * Controller that provides information about Battles
 */
@RestController("battleRestController")
@RequestMapping("/battles")
public class BattleRestController {

  public static final String WIN = "WIN";
  public static final String LOSE = "LOSE";
  private Logger logger = LoggerUtil.INSTANCE.getLogger();

  @Inject
  private BattleRepository battleRepository;

  @Inject
  private Mapper mapper;

  @RequestMapping(value = "/{allianceWinner}/{allianceLoser}/{startDate}/{endDate}/{recaptcha}")
  public Map<String, List<AllianceBattleDTO>> findByBattleAlliancesBetweenDaates(@PathVariable String allianceWinner,
                                                                                 @PathVariable String allianceLoser,
                                                                                 @PathVariable @DateTimeFormat(pattern = "dd-MM-yyyy") Date startDate,
                                                                                 @PathVariable @DateTimeFormat(pattern = "dd-MM-yyyy") Date endDate,
                                                                                 @PathVariable String recaptcha) {

    logger.info("Parameters -  AllianceWinner: {}, Alliance loser: {}, Start date: {}, End date: {}",
      allianceWinner, allianceLoser, startDate, endDate);
    List<AllianceBattleDTO> battles = new ArrayList<>();

    logger.info("Mapper instance: " + mapper);

    battles.addAll(getBattles(allianceWinner, allianceLoser, startDate, endDate, true));


    battles.addAll(getBattles(allianceLoser, allianceWinner, startDate, endDate, false));

    logger.info("Battles size: {}", battles.size());

    Map<String, List<AllianceBattleDTO>> p = new HashMap<>();
    p.put(Constant.JsonConstant.DATA_TABLE_KEYWORD.getKeyWord(), battles);
    return p;
  }

  private List<AllianceBattleDTO> getBattles(String allianceWinner, String allianceLoser, Date startDate, Date endDate, boolean win) {
    List<AllianceBattleDTO> battlesDTO;
    List<Battle> battles = battleRepository.findByAllianceWinnerTagAndAllianceLoserTagAndBattleDateIsBetween(
      allianceWinner, allianceLoser, startDate, endDate);


    battlesDTO = battles.stream()
      .map(b -> {
        AllianceBattleDTO result = mapper.map(b, AllianceBattleDTO.class);
        result.setWin(win);
        return result;
      }).collect(Collectors.toList());

    return battlesDTO;
  }

}
