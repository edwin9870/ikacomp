package com.ikacomp.web.rest;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.Date;

/**
 * Created by Edwin Ramirez on 5/26/2015.
 */
@RestController("/")
public class RestSample {

  class HolaJson {

    private String hola;
    private Date fecha;

    public HolaJson(String hola, Date fecha) {
      this.hola = hola;
      this.fecha = fecha;
    }

    public Date getFecha() {
      return fecha;
    }

    public void setFecha(Date fecha) {
      this.fecha = fecha;
    }

    public String getHola() {
      return hola;
    }

    public void setHola(String hola) {
      this.hola = hola;
    }
  }

  @RequestMapping(value = "/json", method = RequestMethod.GET)
  public HolaJson hola() {
    return new HolaJson("aaaa", new Date());
  }
}
