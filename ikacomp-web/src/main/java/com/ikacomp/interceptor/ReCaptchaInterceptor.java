package com.ikacomp.interceptor;

import com.ikacomp.recaptcha.ReCaptcha;
import com.ikacomp.util.LoggerUtil;
import org.slf4j.Logger;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Created by Edwin Ramirez on 6/6/2015.
 * <p>
 * Interceptor for validate ReCaptcha
 */
@Component
public class ReCaptchaInterceptor extends HandlerInterceptorAdapter {

  public static final String CAPTCHA_VALID_KEY = "CAPTCHA_VALID";
  private Logger logger = LoggerUtil.INSTANCE.getLogger();


  @Inject
  private ReCaptcha reCaptcha;


  @Override
  public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
    logger.info("Start to execute {} interceptor - PreHanlder", this.getClass().getSimpleName());

    if (request.getSession().getAttribute(CAPTCHA_VALID_KEY) != null
      && (Boolean) request.getSession().getAttribute(CAPTCHA_VALID_KEY)) {
      logger.info("Captacha have been validated before, no necesary validate again");
       return true;
    }

    logger.info("Rechaptca key: {}", reCaptcha.getSecretKey());

    String ip = request.getRemoteAddr();
    StringBuffer requestURL = request.getRequestURL();
    String reCaptchaUserAnswer = requestURL.substring(requestURL.lastIndexOf("/") + 1);


    reCaptcha.setRemoteIp(ip);
    reCaptcha.setgCaptcha(reCaptchaUserAnswer);

    boolean isValid = reCaptcha.isValid();

    logger.info("isValid answer for reCaptcha: {}", isValid);
    logger.info("Finish to execute {} interceptor - PreHanlder", this.getClass().getSimpleName());

    if (isValid) {
      request.getSession().setAttribute(CAPTCHA_VALID_KEY, true);
    } else {
      request.getSession().setAttribute(CAPTCHA_VALID_KEY, false);
    }
    return isValid;
  }
}
