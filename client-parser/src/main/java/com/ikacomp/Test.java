package com.ikacomp;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * Created by yoya on 2/14/15.
 */
public class Test {

	private static final int COMBAT_AUTO_REPORT_MINIMIMUM_SIZE = 7;

	public static void main(String []args) throws IOException, ParseException {
		String dateString = "Feb 9th 2015, 5:56pm";
		DateFormat df = new SimpleDateFormat("MMM dd'th' yyyy, hh:mma");
		Date date = df.parse(dateString);

		System.out.println("Date: "+date);
	}

	private static void ex() throws IOException {
		final Document document = Jsoup.connect("http://board.es.ikariam.gameforge.com/index.php?page=Board&boardID=109&&pageNo=2").get();
		String baseUri = document.baseUri();
		System.out.println("Location: "+ baseUri);

		Elements titles = document.select("div#normalThreadsStatus table.tableList p[id^=threadTitle]");

		for(Element title: titles) {
			String battlePrefix = title.select("span strong").text();
//			System.out.print(battlePrefix);

			String url = baseUri+title.select("a").attr("href");
//			System.out.println("href: "+ url);
			Document documentThread = Jsoup.connect(url).get();

			Elements compacterCombatAutoReport = documentThread.select("div.messageContent span > strong");
			if(compacterCombatAutoReport.size() <= COMBAT_AUTO_REPORT_MINIMIMUM_SIZE) {
				continue;
			}


			System.out.print(documentThread.select("div.messageContent strong > span > span[style=color: red]").text());
			System.out.print(" VS ");
			System.out.println(documentThread.select("div.messageContent strong > span > span[style=color: green]").text());

		}
	}
}
