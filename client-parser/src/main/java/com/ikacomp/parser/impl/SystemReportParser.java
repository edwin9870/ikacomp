package com.ikacomp.parser.impl;

import com.google.common.base.Joiner;
import com.google.common.base.Splitter;
import com.ikacomp.Constant;
import com.ikacomp.entities.ikariam.game.Alliance;
import com.ikacomp.entities.ikariam.game.Player;
import com.ikacomp.parser.interfaces.ParticipantBattle;
import com.ikacomp.util.LoggerUtil;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.slf4j.Logger;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

@Component
public class SystemReportParser implements ParticipantBattle {
  private Logger logger = LoggerUtil.INSTANCE.getLogger();

  @Override
  public boolean isValidFormat(Document document) {
    Elements compacterCombatAutoReport = document.select("strong > span[style=font-family: Courier New]");
    if (compacterCombatAutoReport.size() < Constant.CombatReport.SYSTEM_REPORT.getSizeLimit()) {
      return false;
    }

    return true;
  }

  @Override
  public Set<Player> getAttackers(Document document) {

    Set<Player> players = new HashSet<>();

    Elements compacterCombatAutoReport = document.select("strong > span[style=font-family: Courier New]");
    if (compacterCombatAutoReport.size() < Constant.CombatReport.SYSTEM_REPORT.getSizeLimit()) {
      return players;
    }


    final Element domSelectAttacker = document.select("strong span[style=color: #700000]").first();
    String textPlayersBattle = formatPlayersBattle(domSelectAttacker.text());
    final List<String> attackersBattle = Splitter.onPattern(Constant.Regex.COMBAT_SEPARATOR.getRegex()).trimResults().splitToList(textPlayersBattle);
    players = attackersBattle
      .stream()
      .map(playerString -> {
        Player player = new Player();


        player.setAlliance(allianceFromPlayerAlliance(playerString));
        playerString = replaceAllianceTagFromString(playerString, player.getAlliance().getAbbreviation());
        player.setName(playerString);

        return player;
      })
      .collect(Collectors.toSet());

    logger.debug("Attackers: {}", players);

    return players;
  }

  private Alliance allianceFromPlayerAlliance(String playerName) {
    Alliance alliance = new Alliance();
    if (playerName.contains("[") && playerName.contains("]")) {

      final String allianceTag = playerName.substring(playerName.indexOf("[") + 1, playerName.indexOf("]"));
      logger.debug("Alliance tag: {}", allianceTag);
      alliance.setAbbreviation(allianceTag);
      return alliance;
    }

    return alliance;
  }

  private String replaceAllianceTagFromString(String playerString, String allianceTag) {
    playerString = playerString.replaceAll("\\[" + allianceTag + "\\]", "");
    return playerString;
  }

  @Override
  public Set<Player> getDefenders(Document document) {

    Set<Player> players = new HashSet<>();

    Elements compacterCombatAutoReport = document.select("strong > span[style=font-family: Courier New]");
    if (compacterCombatAutoReport.size() < Constant.CombatReport.SYSTEM_REPORT.getSizeLimit()) {
      return players;
    }


    final Element domSelectAttacker = document.select("strong span[style=color: #007000]").first();
    String textPlayersBattle = formatPlayersBattle(domSelectAttacker.text());
    final List<String> deffendersBattle = Splitter.onPattern(Constant.Regex.COMBAT_SEPARATOR.getRegex()).trimResults().splitToList(textPlayersBattle);
    players = deffendersBattle
      .stream()
      .map(playerString -> {
        Player player = new Player();
        player.setAlliance(allianceFromPlayerAlliance(playerString));
        playerString = replaceAllianceTagFromString(playerString, player.getAlliance().getAbbreviation());
        player.setName(playerString);

        return player;
      })
      .collect(Collectors.toSet());

    logger.debug("Defenders: {}", players);
    return players;
  }

  @Override
  public Set<Player> getWinners(Document document) {
    Set<Player> players = new HashSet<>();
    boolean hasWinner = true;
    boolean hasLoser = false;
    String winnerString = "Ganador: ";
    String loserString = "Perdedor: ";

    Elements compacterCombatAutoReport = document.select("strong > span[style=font-family: Courier New]");
    if (compacterCombatAutoReport.size() < Constant.CombatReport.SYSTEM_REPORT.getSizeLimit()) {
      return players;
    }


    final Element domSelectWinners = document.select("strong span[style=color: #000070]").first();
    if (domSelectWinners == null) {
      logger.debug("domSelectWinners is null. Stop procesing the current request");
      return players;
    }
    String textPlayersBattle = domSelectWinners.text();

    if (textPlayersBattle.contains(loserString) && !textPlayersBattle.contains(winnerString)) {
      hasLoser = true;
      hasWinner = false;
    }

    logger.debug("textPlayerBattle: " + textPlayersBattle);
    final List<String> winners = Splitter.onPattern(Constant.Regex.COMBAT_SEPARATOR.getRegex()).trimResults().splitToList(textPlayersBattle);
    players = winners
      .stream()
      .map(playerString -> {
        Player player = new Player();
        playerString = playerString.replace(winnerString, "");
        playerString = playerString.replace(loserString, "");
        player.setAlliance(allianceFromPlayerAlliance(playerString));
        playerString = replaceAllianceTagFromString(playerString, player.getAlliance().getAbbreviation());
        player.setName(playerString);

        return player;
      })
      .collect(Collectors.toSet());

    if (hasLoser) {
      Set<Player> attackers = this.getAttackers(document);
      Set<Player> defenders = this.getDefenders(document);
      Player loserPlayer = players.iterator().next();

      if (attackers.contains(loserPlayer) && !defenders.contains(loserPlayer)) {
        players = defenders;
      } else {
        players = attackers;
      }
    }

    logger.debug("Winners: {}", players);
    return players;
  }

  private String formatPlayersBattle(String rawTest) {
    List<String> listUser = new ArrayList<>();
    Pattern pattern = Pattern.compile(Constant.Regex.PLAYER_SYSTEM_REPORTER.getRegex());
    Matcher matcher = pattern.matcher(rawTest);


    logger.debug("RawTest: {}", rawTest);
    while (matcher.find()) {
      listUser.add(matcher.group(1).trim());
    }
    rawTest = Joiner.on(",").skipNulls().join(listUser);
    return rawTest;
  }
}