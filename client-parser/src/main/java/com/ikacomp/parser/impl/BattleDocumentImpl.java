package com.ikacomp.parser.impl;

import com.ikacomp.Constant;
import com.ikacomp.entities.ikariam.board.Battle;
import com.ikacomp.parser.interfaces.*;
import com.ikacomp.util.LoggerUtil;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.slf4j.Logger;
import org.springframework.stereotype.Component;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;


@Component
public class BattleDocumentImpl implements BattleDocument, TitleBattle, PrefixBattle,
    DateBattle, URLBattle {

  private Logger logger = LoggerUtil.INSTANCE.getLogger();


  @Override
  public List<Battle> getBattles(Document document) {
    Battle battle;

    logger.info("Iniciada capacidad getBattles");

    Elements titles = document.select("div#normalThreadsStatus table.tableList p[id^=threadTitle]");

    logger.debug("Cantidad de titulos de batalla encontrado: {}", titles.size());
    List<Battle> battleList = new ArrayList<>();

    for (Element title : titles) {
      String battlePrefix = getBattlePrefix(title);
      String battleTitle = getTitle(title);
      Date date = getBattleDate(title.parent().parent());
      String urlBattle = getUrlBattle(title);

      battle = new Battle(battlePrefix, battleTitle, date, urlBattle);

      battleList.add(battle);
      logger.info("Agregado objeto: {}", battle);
    }

    logger.info("Finalizada ejecucion de metodo getBattles()");
    return battleList;
  }

  @Override
  public String getTitle(Element element) {
    return element.select("a").text();
  }


  /**
   * Private method for format String date. Ex:
   * (Feb 3rd 2015, 5:51pm)
   * Output:
   * Feb 3rd 2015, 5:51pm
   *
   * @param textDate String date to format
   * @return String date formated
   */
  private String formatHtmlString(String textDate) {
    textDate = textDate.substring(textDate.indexOf("(") + 1);
    textDate = textDate.substring(0, textDate.length() - 1);
    return textDate;
  }


  /**
   * Private method for parse String date to Date.
   * Example input:
   * Feb 3rd 2015, 5:51pm
   *
   * @param textDate to format
   * @return A parsed date object
   */
  private Date parseDate(String textDate) {
    textDate = textDate.toUpperCase();
    Date date;


    String format = "MMM dd'TH' yyyy, hh:mma";
    if (textDate.contains(Constant.OrdinalDaySuffix.ST.getOrdinalDay())) {
      format = "MMM dd'ST' yyyy, hh:mma";
    } else if (textDate.contains(Constant.OrdinalDaySuffix.ND.getOrdinalDay())) {
      format = "MMM dd'ND' yyyy, hh:mma";
    } else if (textDate.contains(Constant.OrdinalDaySuffix.RD.getOrdinalDay())) {
      format = "MMM dd'RD' yyyy, hh:mma";
    }

    DateFormat df = new SimpleDateFormat(format);
    textDate = formatStringDay(textDate.toUpperCase(), df);

    try {
      date = df.parse(textDate);
      return date;
    } catch (ParseException e) {
      return null;
    }
  }

  private String formatStringDay(String textDate, DateFormat df) {
    Date actualDate = new Date();
    Calendar calendar = Calendar.getInstance();

    calendar.setTime(actualDate);
    if (textDate.contains(Constant.Days.YESTERDAY.getDate())) {
      logger.debug("Batalla contiene String yesterday. Date recibida: {}", textDate);
      calendar.setTime(actualDate);
      calendar.add(Calendar.DAY_OF_MONTH, -1);
      actualDate.setTime(calendar.getTime().getTime());
      textDate = textDate.replaceAll(Constant.Days.YESTERDAY.getDate(), df.format(actualDate));
      logger.debug("String textDate formateada: {}", textDate);
    } else if (textDate.contains(Constant.Days.TODAY.getDate())) {
      logger.debug("Batalla contiene String Today. Date recibida: {}", textDate);
      textDate = textDate.replaceAll(Constant.Days.TODAY.getDate(), df.format(actualDate));
      logger.debug("String textDate formateada: {}", textDate);
    }

    return textDate;
  }

  @Override
  public String getBattlePrefix(Element element) {
    return element.select("span strong").text();
  }

  @Override
  public Date getBattleDate(Element element) {
    String dateText = element.select("p.firstPost").text();
    dateText = formatHtmlString(dateText);

    return parseDate(dateText);
  }

  @Override
  public String getUrlBattle(Element element) {

    return element.ownerDocument().baseUri() + element.select("a").attr("href");
  }
}
