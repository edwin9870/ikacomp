package com.ikacomp.parser.impl;

import com.ikacomp.parser.interfaces.ParticipantBattle;
import com.ikacomp.util.LoggerUtil;
import org.jsoup.nodes.Document;
import org.slf4j.Logger;
import org.springframework.stereotype.Component;

@Component
public class ReportParserFactory {

  private static Logger logger = LoggerUtil.INSTANCE.getLogger();

  public static ParticipantBattle getInstance(Document document) {
    logger.info("ReportParserFactory init");


    ParticipantBattle reportParser = new SystemReportParser();

    if (reportParser.isValidFormat(document)) {
      logger.debug("returned SystemReportParser object");

      logger.info("ReportParserFactory finish");
      return reportParser;
    }

    reportParser = new WTFReportParser();

    if (reportParser.isValidFormat(document)) {
      logger.debug("returned WTFReportParser object");

      logger.info("ReportParserFactory finish");
      return reportParser;
    }

    logger.info("ReportParserFactory null returned - finish");
    return null;
  }
}
