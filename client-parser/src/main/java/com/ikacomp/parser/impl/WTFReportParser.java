package com.ikacomp.parser.impl;

import com.google.common.base.Splitter;
import com.ikacomp.Constant;
import com.ikacomp.entities.ikariam.game.Alliance;
import com.ikacomp.entities.ikariam.game.Player;
import com.ikacomp.parser.interfaces.ParticipantBattle;
import com.ikacomp.util.LoggerUtil;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.slf4j.Logger;
import org.springframework.stereotype.Component;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import static com.ikacomp.Constant.*;
import static com.ikacomp.Constant.CombatReport.*;

@Component
public class WTFReportParser implements ParticipantBattle {
  private Logger logger = LoggerUtil.INSTANCE.getLogger();

  @Override
  public boolean isValidFormat(Document document) {
    Elements compacterCombatAutoReport = document.select("div.messageContent span > strong");
    return compacterCombatAutoReport.size() >= WTF_REPORT.getSizeLimit();

  }

  @Override
  public Set<Player> getAttackers(Document document) {

    Set<Player> players = new HashSet<>();

    Elements compacterCombatAutoReport = document.select("div.messageContent span > strong");
    if (compacterCombatAutoReport.size() < WTF_REPORT.getSizeLimit()) {
      return players;
    }


    final Elements domSelectAttacker = document.select("div.messageContent strong > span > span[style=color: red]");
    final List<String> attackersBattle = Splitter.onPattern(Regex.COMBAT_SEPARATOR.getRegex()).trimResults().splitToList(domSelectAttacker.text());
    players = attackersBattle
              .stream()
              .map(playerString -> {
                  Player player = new Player();
                  player.setAlliance(allianceFromPlayerAlliance(playerString));
                  playerString = replaceAllianceTagFromString(playerString,
                                  player.getAlliance().getAbbreviation());
                  player.setName(playerString);

                  return player;
            })
            .collect(Collectors.toSet());

    logger.debug("Attackers: {}", players);

    return players;
  }

  private Alliance allianceFromPlayerAlliance(String playerName) {
    Alliance alliance = new Alliance();
    if (playerName.contains("[") && playerName.contains("]")) {

      final String allianceTag = playerName.substring(playerName.indexOf("[") + 1, playerName.indexOf("]"));
      logger.debug("Alliance tag: {}", allianceTag);
      alliance.setAbbreviation(allianceTag);
      return alliance;
    }

    return alliance;
  }

  private String replaceAllianceTagFromString(String playerString, String allianceTag) {
    playerString = playerString.replaceAll("\\[" + allianceTag + "\\]", "");
    return playerString;
  }

  @Override
  public Set<Player> getDefenders(Document document) {

    Set<Player> players = new HashSet<>();

    Elements compacterCombatAutoReport = document.select("div.messageContent span > strong");
    if (compacterCombatAutoReport.size() < WTF_REPORT.getSizeLimit()) {
      return players;
    }


    final Elements domSelectAttacker = document.select("div.messageContent strong > span > span[style=color: green]");

    final List<String> deffendersBattle = Splitter.onPattern(Regex.COMBAT_SEPARATOR.getRegex()).trimResults().splitToList(domSelectAttacker.text());
    players = deffendersBattle
            .stream()
              .map(playerString -> {
                  Player player = new Player();
                  player.setAlliance(allianceFromPlayerAlliance(playerString));
                  playerString = replaceAllianceTagFromString(playerString, player.getAlliance().getAbbreviation());
                  player.setName(playerString);

                  return player;
                })
            .collect(Collectors.toSet());

    logger.debug("Defenders: {}", players);

    return players;
  }

  @Override
  public Set<Player> getWinners(Document document) {
    Set<Player> players = new HashSet<>();

    Elements compacterCombatAutoReport = document.select("div.messageContent span > strong");
    if (compacterCombatAutoReport.size() < WTF_REPORT.getSizeLimit()) {
      return players;
    }


    final Element domSelectWinners = document.select("em span[style] span[style=font-size: 18pt]").first();

    if (domSelectWinners == null) {
      logger.debug("domSelectWinners is null. Stop procesing the current request");
      return players;
    }

    logger.debug("domSelectWinners: {}", domSelectWinners);
    final List<String> winnersBattle = Splitter.onPattern(Regex.COMBAT_SEPARATOR.getRegex()).trimResults().splitToList(domSelectWinners.text());
    players = winnersBattle
            .stream()
              .map(playerString -> {
                  Player player = new Player();
                  player.setAlliance(allianceFromPlayerAlliance(playerString));
                  playerString = replaceAllianceTagFromString(playerString,
                          player.getAlliance().getAbbreviation());
                  player.setName(playerString);

                  return player;
                })
            .collect(Collectors.toSet());

    logger.debug("Winners: {}", players);

    return players;
  }
}