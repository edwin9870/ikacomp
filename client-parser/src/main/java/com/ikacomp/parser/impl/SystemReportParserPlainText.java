package com.ikacomp.parser.impl;

import com.google.common.base.Joiner;
import com.google.common.base.Splitter;
import com.ikacomp.Constant;
import com.ikacomp.entities.ikariam.game.Alliance;
import com.ikacomp.entities.ikariam.game.Player;
import com.ikacomp.parser.interfaces.ParticipantBattle;
import com.ikacomp.util.LoggerUtil;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.slf4j.Logger;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

@Component
public class SystemReportParserPlainText implements ParticipantBattle {
  private Logger logger = LoggerUtil.INSTANCE.getLogger();

  @Override
  public boolean isValidFormat(Document document) {
    ParticipantBattle participantBattle = new WTFReportParser();
    boolean validFormatWTF = participantBattle.isValidFormat(document);

    participantBattle = new SystemReportParser();
    boolean validFormatSystemReport = participantBattle.isValidFormat(document);

    return !validFormatWTF && !validFormatSystemReport;
  }

  @Override
  public Set<Player> getAttackers(Document document) {
    Set<Player> players;

    final Element domSelectAttacker = document.select("div[id^=postText]").first();
    String textPlayersBattle = formatPlayersBattle(getAttackerText(domSelectAttacker.text()));
    final List<String> attackersBattle = Splitter.onPattern(Constant.Regex.PLAYER_SYSTEM_REPORTER.getRegex()).trimResults().splitToList(textPlayersBattle);
    players = attackersBattle
      .stream()
      .map(playerString -> {
        Player player = new Player();
        player.setAlliance(allianceFromPlayerAlliance(playerString));
        playerString = replaceAllianceTagFromString(playerString, player.getAlliance().getAbbreviation());
        player.setName(playerString);

        return player;
      })
      .collect(Collectors.toSet());

    logger.debug("Attackers: {}", players);

    return players;
  }

  private String getAttackerText(String attackerText) {
    String regex = "\\d{2}:\\d{2}\\)([\\w\\W]+)(?=vs)";
    Pattern pattern = Pattern.compile(regex);
    Matcher matcher = pattern.matcher(attackerText);
    String text = "";
    while (matcher.find()) {
      text = matcher.group(1);
      break;
    }
    return text;
  }

  private String getDeffenderText(String deffenderText) {
    String regex = "(?<=vs)([\\w\\W]+)\\s--";
    Pattern pattern = Pattern.compile(regex);
    Matcher matcher = pattern.matcher(deffenderText);
    String text = "";
    while (matcher.find()) {
      text = matcher.group(1);
      break;
    }
    return text;
  }

  private String getWinnerText(String winnerText, String regex) {
    Pattern pattern = Pattern.compile(regex);
    Matcher matcher = pattern.matcher(winnerText);
    String text = "";
    while (matcher.find()) {
      text = matcher.group(1);
      break;
    }
    return text;
  }


  private Alliance allianceFromPlayerAlliance(String playerName) {
    Alliance alliance = new Alliance();
    if (playerName.contains("[") && playerName.contains("]")) {

      final String allianceTag = playerName.substring(playerName.indexOf("[") + 1, playerName.indexOf("]"));
      logger.debug("Alliance tag: {}", allianceTag);
      alliance.setAbbreviation(allianceTag);
      return alliance;
    }

    return alliance;
  }

  private String replaceAllianceTagFromString(String playerString, String allianceTag) {
    playerString = playerString.replaceAll("\\[" + allianceTag + "\\]", "");
    return playerString;
  }

  @Override
  public Set<Player> getDefenders(Document document) {
    Set<Player> players;

    final Element domSelectAttacker = document.select("div[id^=postText]").first();
    String textPlayersBattle = formatPlayersBattle(getDeffenderText(domSelectAttacker.text()));
    final List<String> deffendersBattle = Splitter.onPattern(Constant.Regex.COMBAT_SEPARATOR.getRegex()).trimResults().splitToList(textPlayersBattle);
    players = deffendersBattle
      .stream()
      .map(playerString -> {
        Player player = new Player();
        player.setAlliance(allianceFromPlayerAlliance(playerString));
        playerString = replaceAllianceTagFromString(playerString, player.getAlliance().getAbbreviation());
        player.setName(playerString);

        return player;
      })
      .collect(Collectors.toSet());

    logger.debug("Defenders: {}", players);
    return players;
  }

  @Override
  public Set<Player> getWinners(Document document) {
    Set<Player> players = new HashSet<>();
    boolean hasLoser = false;
    String winnerString = "<br> Ganador: ";
    String loserString = "<br> Perdedor: ";


    final Element domSelectWinners = document.select("div[id^=postText]").first();
    String textPlayersBattle = domSelectWinners.toString();

    if (textPlayersBattle.contains(loserString) && !textPlayersBattle.contains(winnerString)) {
      hasLoser = true;
    }

    if (hasLoser == false) {
      logger.debug("gettingWinner text Ganador");
      textPlayersBattle = getWinnerText(textPlayersBattle, "(<br> Ganador: [^<]+)");
    } else {
      logger.debug("gettingWinner text perdedor");
      textPlayersBattle = getWinnerText(textPlayersBattle, "(<br> Perdedor: [^<]+)");
    }

    logger.debug("textPlayerBattle: " + textPlayersBattle);
    final List<String> winners = Splitter.onPattern(Constant.Regex.COMBAT_SEPARATOR.getRegex()).trimResults().splitToList(textPlayersBattle);
    players = winners
      .stream()
      .map(playerString -> {
        Player player = new Player();
        playerString = playerString.replace(winnerString, "");
        playerString = playerString.replace(loserString, "");
        player.setAlliance(allianceFromPlayerAlliance(playerString));
        playerString = replaceAllianceTagFromString(playerString, player.getAlliance().getAbbreviation());
        player.setName(playerString);

        return player;
      })
      .collect(Collectors.toSet());

    if (hasLoser) {
      Set<Player> attackers = this.getAttackers(document);
      Set<Player> defenders = this.getDefenders(document);
      Player loserPlayer = players.iterator().next();

      if (attackers.contains(loserPlayer) && !defenders.contains(loserPlayer)) {
        players = defenders;
      } else {
        players = attackers;
      }
    }

    logger.debug("Winners: {}", players);
    return players;
  }

  private String formatPlayersBattle(String rawTest) {
    List<String> listUser = new ArrayList<>();
    Pattern pattern = Pattern.compile(Constant.Regex.PLAYER_SYSTEM_REPORTER.getRegex());
    Matcher matcher = pattern.matcher(rawTest);


    logger.debug("RawTest: {}", rawTest);
    while (matcher.find()) {
      listUser.add(matcher.group(1).trim());
    }
    logger.info("Result formatPlayerBattle: " + listUser);
    rawTest = Joiner.on(",").skipNulls().join(listUser);
    return rawTest;
  }
}