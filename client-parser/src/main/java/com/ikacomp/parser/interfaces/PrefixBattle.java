package com.ikacomp.parser.interfaces;

import org.jsoup.nodes.Element;

/**
 * Created by Edwin Ramirez Ventura on 2/15/15.
 */
public interface PrefixBattle {

	String getBattlePrefix(Element element);
}
