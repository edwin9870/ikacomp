package com.ikacomp.parser.interfaces;

import org.jsoup.nodes.Element;

import java.util.Date;

/**
 * Created by yoya on 2/15/15.
 */
public interface DateBattle {

	Date getBattleDate(Element element);
}
