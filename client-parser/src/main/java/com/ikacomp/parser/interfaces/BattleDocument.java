package com.ikacomp.parser.interfaces;

import com.ikacomp.entities.ikariam.board.Battle;
import org.jsoup.nodes.Document;

import java.util.List;

/**
 * Created by yoya on 2/15/15.
 */
public interface BattleDocument {

	List<Battle> getBattles(Document document);
}
