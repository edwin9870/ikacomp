package com.ikacomp.parser.interfaces;

import org.jsoup.nodes.Element;

/**
 * Created by Edwin Ramirez on 2/14/15.
 */
public interface TitleBattle {

	String getTitle(Element Element);


}
