package com.ikacomp.parser.interfaces;

import com.ikacomp.entities.ikariam.game.Player;
import org.jsoup.nodes.Document;

import java.util.Set;

/**
 * Created by yoya on 2/21/15.
 */
public interface ParticipantBattle {

	boolean isValidFormat(Document document);
	Set<Player> getAttackers(Document document);
	Set<Player> getDefenders(Document document);
	Set<Player> getWinners(Document document);

}
