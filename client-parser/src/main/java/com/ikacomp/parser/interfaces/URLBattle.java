package com.ikacomp.parser.interfaces;

import org.jsoup.nodes.Element;

/**
 * Created by yoya on 2/15/15.
 */
public interface URLBattle {
  String getUrlBattle(Element element);
}
