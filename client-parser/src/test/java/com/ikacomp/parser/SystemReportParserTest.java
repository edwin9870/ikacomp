package com.ikacomp.parser;

import com.google.common.base.Charsets;
import com.google.common.io.CharStreams;
import com.ikacomp.Constant;
import com.ikacomp.entities.ikariam.game.Player;
import com.ikacomp.parser.impl.SystemReportParser;
import com.ikacomp.parser.interfaces.ParticipantBattle;
import com.ikacomp.util.LoggerUtil;
import org.jsoup.Jsoup;
import org.junit.Test;
import org.slf4j.Logger;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Set;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

public class SystemReportParserTest {

	private static final int PRIMER_ELEMENTO = 0;

	private Logger logger = LoggerUtil.INSTANCE.getLogger();

	@Test
	public void testGetAttackers() throws Exception {
		ParticipantBattle participantBattle = new SystemReportParser();


		for (String report : Constant.HtmlFileTest.SYSTEM.getDocuments()) {
			InputStream inputStream = Thread.currentThread().getContextClassLoader().getResourceAsStream(report);

			assertNotNull("El objeto inputStream no puede ser nulo", inputStream);
			final String html = CharStreams.toString(new InputStreamReader(inputStream, Charsets.UTF_8));
			final Set<Player> attackers = participantBattle.getAttackers(Jsoup.parse(html));

			assertFalse("Fail because the parser could not get the attakers list", attackers.isEmpty());
		}

	}

	@Test
	public void testGetDefenders() throws Exception {
		ParticipantBattle participantBattle = new SystemReportParser();

		for (String report : Constant.HtmlFileTest.SYSTEM.getDocuments()) {
			InputStream inputStream = Thread.currentThread().getContextClassLoader().getResourceAsStream(report);

			assertNotNull("El objeto inputStream no puede ser nulo", inputStream);
			final String html = CharStreams.toString(new InputStreamReader(inputStream, Charsets.UTF_8));
			final Set<Player> defenders = participantBattle.getDefenders(Jsoup.parse(html));

			assertFalse("Fail because the parser could not get the defenders list", defenders.isEmpty());
		}
	}

	@Test
	public void testWinners() throws Exception {
		ParticipantBattle participantBattle = new SystemReportParser();

		for (String report : Constant.HtmlFileTest.SYSTEM.getDocuments()) {
			InputStream inputStream = Thread.currentThread().getContextClassLoader().getResourceAsStream(report);

			assertNotNull("El objeto inputStream no puede ser nulo", inputStream);
			final String html = CharStreams.toString(new InputStreamReader(inputStream, Charsets.UTF_8));

			final Set<Player> winners = participantBattle.getWinners(Jsoup.parse(html));

			assertFalse("Fail because the parser could not get the getWinners list", winners.isEmpty());
		}

	}

	@Test
	public void testIsNotValidFormat() throws Exception {
		final ParticipantBattle participantBattle = new SystemReportParser();
		final InputStream inputStream = Thread.currentThread().getContextClassLoader().getResourceAsStream(Constant.HtmlFileTest.WTF.getDocuments()[PRIMER_ELEMENTO]);
		final String html = CharStreams.toString(new InputStreamReader(inputStream, Charsets.UTF_8));

		assertFalse("El formato de la batalla debe ser invalido porque el formato corresponde a uno invalido", participantBattle.isValidFormat(Jsoup.parse(html)));
	}

	@Test
	public void testIsValidFormat() throws Exception {
		final ParticipantBattle participantBattle = new SystemReportParser();
		final InputStream inputStream = Thread.currentThread().getContextClassLoader().getResourceAsStream(Constant.HtmlFileTest.SYSTEM.getDocuments()[PRIMER_ELEMENTO]);
		final String html = CharStreams.toString(new InputStreamReader(inputStream, Charsets.UTF_8));

		assertTrue("El formato de la batalla debe ser valido porque el formato corresponde a uno valido",participantBattle.isValidFormat(Jsoup.parse(html)));
	}
}
