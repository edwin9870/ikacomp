package com.ikacomp.parser;

import com.ikacomp.Constant;
import com.ikacomp.entities.ikariam.board.Battle;
import com.ikacomp.parser.impl.BattleDocumentImpl;
import com.ikacomp.parser.interfaces.BattleDocument;
import com.ikacomp.parser.interfaces.DateBattle;
import com.ikacomp.util.LoggerUtil;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.junit.Test;
import org.slf4j.Logger;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

public class BattleDocumentImplTest {

	private static final String URL_BATTLE = "http://board.es.ikariam.gameforge.com/index.php?page=Board&boardID=109";
	private static final int TIMEOUT = 5 * 3600;
	private static final int YESTERDAY;
	private static final int ACTUAL_DAY;

	static {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(new Date());
		ACTUAL_DAY = calendar.get(Calendar.DAY_OF_MONTH);

		calendar.add(Calendar.DAY_OF_MONTH,-1);
		YESTERDAY = calendar.get(Calendar.DAY_OF_MONTH);
	}

	private Logger logger = LoggerUtil.INSTANCE.getLogger();

	@Test
	public void testGetBattles() throws Exception {

		BattleDocument documentBattle = new BattleDocumentImpl();
		Document document = Jsoup.connect(URL_BATTLE).userAgent(Constant.UserAgent.FIREFOX.getUserAgent()).timeout(TIMEOUT).get();


		List<Battle> battles = documentBattle.getBattles(document);
		assertNotNull(battles);
	}

	@Test
	public void testYesterdayGetBattleDate() throws Exception {
		DateBattle dateBattle = new BattleDocumentImpl();

		String html = "<p class=\"firstPost light\"> By <a href=\"index.php?page=User&amp;userID=46439&amp;s=3f2fb2c8fe34d98958f4745855c8f51da3568989\">JEROE</a> (Yesterday, 11:17pm)</p>";
		Document doc = Jsoup.parse(html);

		Date date = dateBattle.getBattleDate(doc.body());
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		assertTrue("The date returned shuloud by yesterday. "+YESTERDAY, calendar.get(Calendar.DAY_OF_MONTH) == YESTERDAY);
	}

	@Test
	public void testTodayGetBattleDate() throws Exception {
		DateBattle dateBattle = new BattleDocumentImpl();

		String html = "<p class=\"firstPost light\"> By <a href=\"index.php?page=User&amp;userID=46439&amp;s=3f2fb2c8fe34d98958f4745855c8f51da3568989\">JEROE</a> (<b>Today</b>, 12:18am)</p>";
		Document doc = Jsoup.parse(html);

		Date date = dateBattle.getBattleDate(doc.body());
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		assertTrue("The date returned should return the actual day", calendar.get(Calendar.DAY_OF_MONTH) == ACTUAL_DAY);
	}

}