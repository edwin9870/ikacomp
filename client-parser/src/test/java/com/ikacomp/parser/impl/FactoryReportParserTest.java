package com.ikacomp.parser.impl;

import com.google.common.base.Charsets;
import com.google.common.io.CharStreams;
import com.ikacomp.parser.interfaces.ParticipantBattle;
import junit.framework.TestCase;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.junit.Test;

import java.io.File;
import java.io.InputStream;
import java.io.InputStreamReader;

public class FactoryReportParserTest extends TestCase {

    private static final String SYSTEM_REPORT_DIR = File.separator + "battle" + File.separator + "reporter" + File.separator + "system" + File.separator;
    private static final String[] SYSTEM_COMVERTER = {SYSTEM_REPORT_DIR + "system-converter.html", SYSTEM_REPORT_DIR + "twoAttackerVsTwoDefenders.html", SYSTEM_REPORT_DIR + "2Vs7.html"};
    private static final String WTF_REPORT_DIR = File.separator + "battle" + File.separator + "reporter" + File.separator + "wtf" + File.separator;
    private static final String[] COMBAT_REPORT_COMVERTER = {WTF_REPORT_DIR + "CombatReportAuto-Converter.html", WTF_REPORT_DIR + "1vs1.html", WTF_REPORT_DIR + "16vs20notally.html"};


    @Test
    public void testGetInstanceSystemReportParser() throws Exception {
        ParticipantBattle participantBattle = null;
        for (String report : SYSTEM_COMVERTER) {
            InputStream inputStream = Thread.currentThread().getContextClassLoader().getResourceAsStream(report);
            assertNotNull("El objeto inputStream no puede ser nulo", inputStream);

            final String html = CharStreams.toString(new InputStreamReader(inputStream, Charsets.UTF_8));
            Document document = Jsoup.parse(html);

            participantBattle = ReportParserFactory.getInstance(document);
            assertNotNull("El objeto participantBattle no puede ser nulo", inputStream);

            assertTrue("Fail because participantBattle should be a instance of SystemReportParser", participantBattle instanceof SystemReportParser);
        }
    }


    @Test
    public void testGetInstanceWTFReportParser() throws Exception {
        ParticipantBattle participantBattle = null;
        for (String report : COMBAT_REPORT_COMVERTER) {
            InputStream inputStream = Thread.currentThread().getContextClassLoader().getResourceAsStream(report);
            assertNotNull("El objeto inputStream no puede ser nulo", inputStream);

            final String html = CharStreams.toString(new InputStreamReader(inputStream, Charsets.UTF_8));
            Document document = Jsoup.parse(html);

            participantBattle = ReportParserFactory.getInstance(document);
            assertNotNull("El objeto participantBattle no puede ser nulo", inputStream);

            assertTrue("Fail because participantBattle should be a instance of WTFReportParser", participantBattle instanceof WTFReportParser);
        }
    }
}