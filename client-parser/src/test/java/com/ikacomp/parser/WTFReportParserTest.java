package com.ikacomp.parser;

import com.google.common.base.Charsets;
import com.google.common.io.CharStreams;
import com.ikacomp.Constant;
import com.ikacomp.entities.ikariam.game.Player;
import com.ikacomp.parser.impl.WTFReportParser;
import com.ikacomp.parser.interfaces.ParticipantBattle;
import com.ikacomp.util.LoggerUtil;
import junit.framework.TestCase;
import org.jsoup.Jsoup;
import org.junit.Test;
import org.slf4j.Logger;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Set;

public class WTFReportParserTest extends TestCase {


	private Logger logger = LoggerUtil.INSTANCE.getLogger();

	public void testGetAttackers() throws Exception {
		ParticipantBattle participantBattle = new WTFReportParser();

		for (String report : Constant.HtmlFileTest.WTF.getDocuments()) {
			InputStream inputStream = Thread.currentThread().getContextClassLoader().getResourceAsStream(report);

			assertNotNull("El objeto inputStream no puede ser nulo", inputStream);
			final String html = CharStreams.toString(new InputStreamReader(inputStream, Charsets.UTF_8));
			final Set<Player> attackers = participantBattle.getAttackers(Jsoup.parse(html));

			assertFalse("Fail because the parser could not get the attakers list", attackers.isEmpty());
		}

	}

	public void testGetDefenders() throws Exception {
		ParticipantBattle participantBattle = new WTFReportParser();

		for (String report : Constant.HtmlFileTest.WTF.getDocuments()) {
			InputStream inputStream = Thread.currentThread().getContextClassLoader().getResourceAsStream(report);

			assertNotNull("El objeto inputStream no puede ser nulo", inputStream);
			final String html = CharStreams.toString(new InputStreamReader(inputStream, Charsets.UTF_8));
			final Set<Player> defenders = participantBattle.getDefenders(Jsoup.parse(html));

			assertFalse("Fail because the parser could not get the defenders list", defenders.isEmpty());
		}
	}

	public void testWinners() throws Exception {
		ParticipantBattle participantBattle = new WTFReportParser();

		for (String report : Constant.HtmlFileTest.WTF.getDocuments()) {
			InputStream inputStream = Thread.currentThread().getContextClassLoader().getResourceAsStream(report);

			assertNotNull("El objeto inputStream no puede ser nulo", inputStream);
			final String html = CharStreams.toString(new InputStreamReader(inputStream, Charsets.UTF_8));

			final Set<Player> winners = participantBattle.getWinners(Jsoup.parse(html));

			assertFalse("Fail because the parser could not get the getWinners list", winners.isEmpty());
		}
	}

	@Test
	public void testIsNotValidFormat() throws Exception {
		final ParticipantBattle participantBattle = new WTFReportParser();
		final InputStream inputStream = Thread.currentThread().getContextClassLoader().getResourceAsStream(Constant.HtmlFileTest.SYSTEM.getDocuments()[0]);
		final String html = CharStreams.toString(new InputStreamReader(inputStream, Charsets.UTF_8));

		assertFalse("El formato de la batalla debe ser invalido porque el formato corresponde a uno invalido", participantBattle.isValidFormat(Jsoup.parse(html)));
	}

	@Test
	public void testIsValidFormat() throws Exception {
		final ParticipantBattle participantBattle = new WTFReportParser();
		final InputStream inputStream = this.getClass().getClassLoader().getResourceAsStream(Constant.HtmlFileTest.WTF.getDocuments()[0]);
		final String html = CharStreams.toString(new InputStreamReader(inputStream, Charsets.UTF_8));

		assertTrue("El formato de la batalla debe ser valido porque el formato corresponde a uno valido", participantBattle.isValidFormat(Jsoup.parse(html)));
	}

    @Test
    public void testing() {
        String url = "http://board.es.ikariam.gameforge.com/index.php?page=Board&boardID=109&pageNo=1";

        for(int i=1;i<20;i++) {
            url = url.substring(0, url.lastIndexOf("=")+1);
            url = url.concat(Integer.toString(i));
            logger.debug(url);
        }

    }

}