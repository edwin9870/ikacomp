package com.ikacomp.dto;

/**
 * Created by Edwin Ramirez on 5/30/2015.
 */
public class AllianceBattleDTO {

  private String title;
  private String url;
  private String serverBoardName;
  private String date;
  private boolean win;

  public AllianceBattleDTO() {
  }

  public AllianceBattleDTO(String title, String url, String serverBoardName) {
    this.title = title;
    this.url = url;
    this.serverBoardName = serverBoardName;
  }

  public String getTitle() {
    return title;
  }

  public void setTitle(String title) {
    this.title = title;
  }

  public String getUrl() {
    return url;
  }

  public void setUrl(String url) {
    this.url = url;
  }

  public String getServerBoardName() {
    return serverBoardName;
  }

  public void setServerBoardName(String serverBoardName) {
    this.serverBoardName = serverBoardName;
  }

  public String getDate() {
    return date;
  }

  public void setDate(String date) {
    this.date = date;
  }

  public boolean isWin() {
    return win;
  }

  public void setWin(boolean win) {
    this.win = win;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;

    AllianceBattleDTO that = (AllianceBattleDTO) o;

    if (!url.equals(that.url)) return false;
    if (serverBoardName != null ? !serverBoardName.equals(that.serverBoardName) : that.serverBoardName != null)
      return false;
    return date.equals(that.date);

  }

  @Override
  public int hashCode() {
    int result = url.hashCode();
    result = 31 * result + (serverBoardName != null ? serverBoardName.hashCode() : 0);
    result = 31 * result + date.hashCode();
    return result;
  }

  @Override
  public String toString() {
    return "AllianceBattleDTO{" +
      "title='" + title + '\'' +
      ", url='" + url + '\'' +
      ", serverBoardName='" + serverBoardName + '\'' +
      ", date='" + date + '\'' +
      '}';
  }
}
