package com.ikacomp;

import java.io.File;


public class Constant {
  public enum UserAgent {

    CHROME("Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_1) "
            + "Safari/537.36" + "AppleWebKit/537.36 (KHTML, like Gecko) "
            +
            "Chrome/41.0.2227.1 "),
    FIREFOX("Firefox/36.0" + "Mozilla/5.0 (Windows NT 6.3; rv:36.0) Gecko/20100101 ");
    private String userAgent;

    UserAgent(String userAgent) {
      this.userAgent = userAgent;
    }

    public String getUserAgent() {
      return userAgent;
    }
  }

  public enum HttpTimeOut {
    TIME_OUT(5000);
    private int timeOut;

    HttpTimeOut(int timeOut) {
      this.timeOut = timeOut;
    }

    public int getTimeOut() {
      return timeOut;
    }
  }

  public enum Days {
    TODAY("TODAY"),
    YESTERDAY("YESTERDAY");
    private String date;

    Days(String date) {
      this.date = date;
    }

    public String getDate() {
      return date;
    }
  }

  public enum CombatReport {
    WTF_REPORT(7),
    SYSTEM_REPORT(1);
    int sizeLimit;

    CombatReport(int sizeLimit) {
      this.sizeLimit = sizeLimit;
    }

    public int getSizeLimit() {
      return sizeLimit;
    }
  }

  public enum HtmlFileTest {
    WTF {
      private final String wtfReportDir = File.separator + "battle"
              + File.separator + "reporter" + File.separator + "wtf"
              + File.separator;
      private final String[] combatReportComverter = {wtfReportDir
              + "CombatReportAuto-Converter.html", wtfReportDir
              + "1vs1.html", wtfReportDir + "16vs20notally.html", wtfReportDir
              + "RamoNix_EDK_VS_Xaubi_NCS.html"};

      @Override
      public String[] getDocuments() {
        return combatReportComverter;
      }
    },
    SYSTEM {
      private final String systemReportDir = File.separator + "battle"
              + File.separator + "reporter" + File.separator + "system" + File.separator;
      private final String[] systemComverter = {systemReportDir
              + "system-converter.html", systemReportDir
              + "twoAttackerVsTwoDefenders.html", systemReportDir
              + "2Vs7.html", systemReportDir + "2Vs1.html", systemReportDir
              + "1Vs2.html", systemReportDir + "2EDK.vs1.html"};

      @Override
      public String[] getDocuments() {
        return systemComverter;
      }
    },
    SYSTEM_PLAIN_TEXT {
      private final String systemReportDir = File.separator + "battle" + File.separator + "reporter"
              + File.separator + "system_plain_text" + File.separator;
      private final String[] systemComverter = {systemReportDir + "2Vs1.html", systemReportDir
              + "5Vs5.html"};

      @Override
      public String[] getDocuments() {
        return systemComverter;
      }
    };

    public abstract String[] getDocuments();
  }

  public enum OrdinalDaySuffix {
    ST("ST"), ND("ND"), RD("RD"), TH("YH");
    private String ordinalDay;

    OrdinalDaySuffix(String ordinalDay) {
      this.ordinalDay = ordinalDay;
    }

    public String getOrdinalDay() {
      return ordinalDay;
    }
  }

  public enum Regex {
    COMBAT_SEPARATOR(",|\\sy\\s"),
    PLAYER_SYSTEM_REPORTER("([\\-a-zA-Z_\\[\\]\\s\\d]+) de");
    String regex;

    Regex(String regex) {
      this.regex = regex;
    }

    public String getRegex() {
      return regex;
    }
  }

  public enum PersistenceUnit {
    IKA_COMP("ika-comp");
    private String persistenName;

    PersistenceUnit(String persistenName) {
      this.persistenName = persistenName;
    }

    public String getName() {
      return persistenName;
    }
  }

  public enum Status {
    AC("AC", "ACEPTADA"), OK("PC", "PROCESADA"), RE("RE", "RECHAZADA"), NP("NP", "NO SOPORTADA");
    private String abbreviacion;
    private String descripcion;

    Status(String abbreviacion, String descripcion) {
      this.abbreviacion = abbreviacion;
      this.descripcion = descripcion;
    }

    public String getAbbreviacion() {
      return abbreviacion;
    }

    public String getDescripcion() {
      return descripcion;
    }

  }

  public enum QueryString {
    THREAD_ID("threadID");
    private String keyName;

    QueryString(String keyName) {
      this.keyName = keyName;
    }

    public String getKeyName() {
      return keyName;
    }
  }

  public enum BattleLimit {
    BATTLE_LIMIT(5);
    private short battleLimit;

    BattleLimit(int battleLimit) {
      this.battleLimit = (short) battleLimit;
    }
    public short getBattleLimit() {
      return battleLimit;
    }
  }

  public enum Date {
    DATE_FORMAT("dd-MM-yyyy"),
    DATETIME_FORMAT("dd-MM-yyyyTHH:mm:ss");
    String format;

    Date(String format) {
      this.format = format;
    }
    public String getFormat() {
      return format;
    }
  }

  public enum JsonConstant{
    DATA_TABLE_KEYWORD("data");
    String keyWord;

    JsonConstant(String keyWord) {
      this.keyWord = keyWord;
    }

    public String getKeyWord() {
      return keyWord;
    }
  }

  public enum URL {
    GOOGLE_RECAPTCHA("https://www.google.com/recaptcha/api/siteverify");
    private String url;

    URL(String url) {
      this.url = url;
    }

    public String getUrl() {
      return url;
    }
  }
}
