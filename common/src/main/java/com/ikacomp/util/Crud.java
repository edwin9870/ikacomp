package com.ikacomp.util;

import org.slf4j.Logger;

import javax.persistence.EntityExistsException;
import javax.persistence.EntityManager;
import java.sql.SQLIntegrityConstraintViolationException;

/**
 * Created by Edwin Ramirez on 3/21/2015.
 */
public class Crud<T> {

    private EntityManager entityManager;
    private Logger logger = LoggerUtil.INSTANCE.getLogger();

    public boolean create(T entity) {
        entityManager = EntityManagerUtil.INSTANCE.getInstance();

        try {
            entityManager.getTransaction().begin();
            logger.info("Entity to insert: {}", entity);
            entityManager.persist(entity);
            entityManager.getTransaction().commit();
        } catch (EntityExistsException e) {
            entityManager.getTransaction().rollback();
            return false;
        }
        return true;
    }

}
