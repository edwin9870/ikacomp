package com.ikacomp.util.sdo;

import com.ikacomp.entities.ikariam.board.Battle;
import org.jsoup.nodes.Document;

/**
 * Created by Edwin Ramirez on 5/23/2015.
 *
 * Class that contains a instance of Battle and Document
 */
public class BattleDocument {

  private Battle battle;
  private Document document;

  public BattleDocument() {
  }

  public BattleDocument(Battle battle, Document document) {
    this.battle = battle;
    this.document = document;
  }

  public Battle getBattle() {
    return battle;
  }

  public Document getDocument() {
    return document;
  }
}
