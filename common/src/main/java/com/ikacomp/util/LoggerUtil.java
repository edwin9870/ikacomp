package com.ikacomp.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by yoya on 2/14/15.
 */
public enum LoggerUtil {

	INSTANCE;
	private Logger logger;

	LoggerUtil() {
		logger = LoggerFactory.getLogger("com.ikacomp");
	}

	public Logger getLogger() {
		return logger;
	}

}
