package com.ikacomp.util;

import com.ikacomp.Constant;

import javax.persistence.EntityManager;
import javax.persistence.Persistence;

/**
 * Created by Edwin Ramirez on 3/15/2015.
 */
public enum EntityManagerUtil {
    INSTANCE;

    private EntityManager entityManager;

    private EntityManagerUtil(){
    }

    public EntityManager getInstance() {
        entityManager = Persistence.createEntityManagerFactory(Constant.PersistenceUnit.IKA_COMP.getName()).createEntityManager();
        return entityManager;
    }

}
