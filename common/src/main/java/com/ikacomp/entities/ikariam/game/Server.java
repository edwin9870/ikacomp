package com.ikacomp.entities.ikariam.game;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

/**
 * Created by yoya on 2/14/15.
 */
@Entity
@Table(name = "server")
@Access(AccessType.PROPERTY)
public class Server implements Serializable {

	private static final long serialVersionUID = 2615990242521302451L;
	
	private int serverId;
	private String name;
	private String abbreviation;
	private List<World> world;

	public Server() {
	}

	@Id @GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "SERVER_ID")
	public int getServerId() {
		return serverId;
	}

	public void setServerId(int serverId) {
		this.serverId = serverId;
	}

	@Column(name = "NAME")
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Column(name = "ABBREVIATION")
	public String getAbbreviation() {
		return abbreviation;
	}

	public void setAbbreviation(String abbreviation) {
		this.abbreviation = abbreviation;
	}

	@ManyToMany(mappedBy = "servers")
	public List<World> getWorld() {
		return world;
	}

	public void setWorld(List<World> world) {
		this.world = world;
	}

	@Override
	public String toString() {
		return "Server{" +
				"serverId=" + serverId +
				", name='" + name + '\'' +
				", abbreviation='" + abbreviation + '\'' +
				", world=" + world +
				'}';
	}
}
