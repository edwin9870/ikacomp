package com.ikacomp.entities.ikariam.game;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

/**
 * Created by yoya on 2/14/15.
 */
@Entity
@Table(name = "player")
@Access(AccessType.PROPERTY)
@NamedQueries(value = {
  @NamedQuery(name = "Player.findByName",
    query = "SELECT p FROM Player p WHERE p.name = :name"),
  @NamedQuery(name = "Player.findByPlayerId",
    query = "SELECT p FROM Player p WHERE p.playerId = :playerId")
})
public class Player implements Serializable {
  private static final long serialVersionUID = -9113431598260677990L;

  private Integer playerId;
  private String name;
  private Alliance alliance;
  private List<Server> servers;


  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "PLAYER_ID")
  public Integer getPlayerId() {
    return playerId;
  }

  public void setPlayerId(Integer playerId) {
    this.playerId = playerId;
  }

  @Column(name = "NAME")
  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  @Transient
  public Alliance getAlliance() {
    return alliance;
  }

  public void setAlliance(Alliance alliance) {
    this.alliance = alliance;
  }

  @ManyToMany
  @JoinTable(
    name = "player_server",
    joinColumns = {@JoinColumn(name = "PLAYER_ID", referencedColumnName = "PLAYER_ID")},
    inverseJoinColumns = {@JoinColumn(name = "SERVER_ID", referencedColumnName = "SERVER_ID")}
  )
  public List<Server> getServers() {
    return servers;
  }

  public void setServers(List<Server> servers) {
    this.servers = servers;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (!(o instanceof Player)) return false;

    Player player = (Player) o;

    if (!name.equals(player.name)) return false;

    return true;
  }

  @Override
  public int hashCode() {
    return name.hashCode();
  }

  @Override
  public String toString() {
    return "Player{" +
      "name='" + name + '\'' +
      ", playerId=" + playerId +
      '}';
  }
}
