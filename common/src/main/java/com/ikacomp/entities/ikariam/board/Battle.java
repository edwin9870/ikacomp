package com.ikacomp.entities.ikariam.board;

import com.ikacomp.entities.ikariam.game.Alliance;
import com.ikacomp.entities.ikariam.game.Player;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.Set;

/**
 * Created by yoya on 2/14/15.
 */
@Entity
@Table(name = "battle")
@Access(AccessType.PROPERTY)
@NamedQueries({
  @NamedQuery(name = "Battle.threadIdCount",
    query = "SELECT COUNT(b.threadId) FROM Battle b WHERE b.threadId = :threadId"),
  @NamedQuery(name = "Battle.findByEstatus",
    query = "SELECT b FROM Battle b WHERE b.status = :status"),
  @NamedQuery(name = "Battle.findByAllianceWinnerTagAndAllianceLoserTagAndBattleDateIsBetween",
    query = "SELECT b FROM Battle b INNER JOIN b.alliancesWinner aw INNER JOIN b.alliancesLoser al WHERE aw.abbreviation = :abbreviationWinner AND al.abbreviation = :abbreviationLoser AND b.date BETWEEN  :startDate AND :endDate"),
  @NamedQuery(name = "Battle.findByAllianceTagAndDateIsBetween",
    query = "SELECT b FROM Battle b INNER JOIN b.alliancesWinner aw INNER JOIN  b.alliancesLoser al WHERE (aw.abbreviation = :abbreviation OR al.abbreviation = :abbreviation) AND b.date BETWEEN  :startDate AND :endDate"),
  @NamedQuery(name = "Battle.threadIdExits",
    query = "SELECT CASE WHEN COUNT(b) > 0 THEN true ELSE false END FROM Battle b WHERE b.threadId = :threadId")
})
public class Battle implements Serializable {

  private static final long serialVersionUID = -4974032177143005787L;

  private Integer battleId;
  private String prefix;
  private String title;
  private Date date;
  private String url;
  private int threadId;
  private String status;
  private ServerBoard serverBoard;
  private Set<Player> winners;
  private Set<Player> attackers;
  private Set<Player> defenders;
  private Set<Alliance> alliancesWinner;
  private Set<Alliance> alliancesLoser;


  public Battle() {
  }

  /**
   * Class constructor with all fields
   *
   * @param prefix
   * @param title
   * @param date
   * @param url
   */
  public Battle(String prefix, String title, Date date, String url) {
    this.prefix = prefix;
    this.title = title;
    this.date = date;
    this.url = url;
  }

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "BATTLE_ID")
  public Integer getBattleId() {
    return battleId;
  }

  public void setBattleId(Integer battleId) {
    this.battleId = battleId;
  }

  @Column(name = "PREFIX")
  public String getPrefix() {
    return prefix;
  }

  public void setPrefix(String prefix) {
    this.prefix = prefix;
  }

  @Column(name = "TITLE")
  public String getTitle() {
    return title;
  }

  public void setTitle(String title) {
    this.title = title;
  }

  @Column(name = "DATE")
  @Temporal(TemporalType.TIMESTAMP)
  public Date getDate() {
    return date;
  }

  public void setDate(Date date) {
    this.date = date;
  }

  @Column(name = "URL", unique = true)
  public String getUrl() {
    return url;
  }

  public void setUrl(String url) {
    this.url = url;
  }

  @Column(name = "THREAD_ID")
  public int getThreadId() {
    return threadId;
  }

  public void setThreadId(int threadId) {
    this.threadId = threadId;
  }

  @Column(name = "STATUS")
  public String getStatus() {
    return status;
  }

  public void setStatus(String status) {
    this.status = status;
  }

  @ManyToOne
  @JoinColumn(name = "SERVER_BOARD_ID", referencedColumnName = "SERVER_BOARD_ID")
  public ServerBoard getServerBoard() {
    return serverBoard;
  }

  public void setServerBoard(ServerBoard serverBoard) {
    this.serverBoard = serverBoard;
  }

  @ManyToMany
  @JoinTable(name = "battle_defender",
    joinColumns = {@JoinColumn(name = "BATTLE_ID", referencedColumnName = "BATTLE_ID", insertable = false, updatable = false)},
    inverseJoinColumns = {@JoinColumn(name = "PLAYER_ID", referencedColumnName = "PLAYER_ID", insertable = false, updatable = false)})
  public Set<Player> getDefenders() {
    return defenders;
  }

  public void setDefenders(Set<Player> defenders) {
    this.defenders = defenders;
  }

  @ManyToMany
  @JoinTable(name = "battle_attacker",
    joinColumns = {@JoinColumn(name = "BATTLE_ID", referencedColumnName = "BATTLE_ID", insertable = false, updatable = false)},
    inverseJoinColumns = {@JoinColumn(name = "PLAYER_ID", referencedColumnName = "PLAYER_ID", insertable = false, updatable = false)})
  public Set<Player> getAttackers() {
    return attackers;
  }

  public void setAttackers(Set<Player> attackers) {
    this.attackers = attackers;
  }

  @ManyToMany
  @JoinTable(name = "battle_winner",
    joinColumns = {@JoinColumn(name = "BATTLE_ID", referencedColumnName = "BATTLE_ID", insertable = false, updatable = false)},
    inverseJoinColumns = {@JoinColumn(name = "PLAYER_ID", referencedColumnName = "PLAYER_ID", insertable = false, updatable = false)})
  public Set<Player> getWinners() {
    return winners;
  }

  public void setWinners(Set<Player> winners) {
    this.winners = winners;
  }

  @ManyToMany
  @JoinTable(name = "alliance_loser",
    joinColumns = {@JoinColumn(name = "BATTLE_ID", referencedColumnName = "BATTLE_ID", insertable = false, updatable = false)},
    inverseJoinColumns = {@JoinColumn(name = "ALLIANCE_ID", referencedColumnName = "ALLIANCE_ID", insertable = false, updatable = false)})
  public Set<Alliance> getAlliancesLoser() {
    return alliancesLoser;
  }

  public void setAlliancesLoser(Set<Alliance> alliancesLoser) {
    this.alliancesLoser = alliancesLoser;
  }

  @ManyToMany
  @JoinTable(name = "alliance_winner",
    joinColumns = {@JoinColumn(name = "BATTLE_ID", referencedColumnName = "BATTLE_ID", insertable = false, updatable = false)},
    inverseJoinColumns = {@JoinColumn(name = "ALLIANCE_ID", referencedColumnName = "ALLIANCE_ID", insertable = false, updatable = false)})
  public Set<Alliance> getAlliancesWinner() {
    return alliancesWinner;
  }

  public void setAlliancesWinner(Set<Alliance> alliancesWinner) {
    this.alliancesWinner = alliancesWinner;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (!(o instanceof Battle)) return false;

    Battle battle = (Battle) o;

    if (!url.equals(battle.url)) return false;

    return true;
  }

  /**
   * Method that return true if battleId or status field are nulls.
   *
   * @return
   */
  @Transient
  public boolean isEmpty() {
    return threadId <= 0 || status == null;

  }

  @Override
  public int hashCode() {
    return url.hashCode();
  }

  @Override
  public String toString() {
    return "Battle{" +
      "battleId=" + battleId +
      ", date=" + date +
      ", attackers=" + attackers +
      ", defenders=" + defenders +
      '}';
  }
}
