package com.ikacomp.entities.ikariam.board;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Created by yoya on 2/14/15.
 */
@Entity
@Access(AccessType.PROPERTY)
@Table(name = "server_board")
@NamedQueries(
  @NamedQuery(name = "ServerBoard.findAll", query = "SELECT s FROM ServerBoard s")
)
public class ServerBoard implements Serializable {

  private static final long serialVersionUID = 1189408075891804130L;

  private Integer serverBoardId;
  private String name;
  private String url;

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "SERVER_BOARD_ID")
  public Integer getServerBoardId() {
    return serverBoardId;
  }

  public void setServerBoardId(Integer serverBoardId) {
    this.serverBoardId = serverBoardId;
  }

  @Column(name = "URL")
  public String getUrl() {
    return url;
  }

  public void setUrl(String url) {
    this.url = url;
  }

  @Column(name = "NAME")
  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;

    ServerBoard that = (ServerBoard) o;

    if (serverBoardId != that.serverBoardId) return false;
    if (name != null ? !name.equals(that.name) : that.name != null)
      return false;
    if (url != null ? !url.equals(that.url) : that.url != null) return false;

    return true;
  }

  @Override
  public int hashCode() {
    int result = serverBoardId;
    result = 31 * result + (name != null ? name.hashCode() : 0);
    result = 31 * result + (url != null ? url.hashCode() : 0);
    return result;
  }

  @Override
  public String toString() {
    return "ServerBoard{" +
      "serverBoardId=" + serverBoardId +
      ", name='" + name + '\'' +
      ", url='" + url + '\'' +
      '}';
  }
}
