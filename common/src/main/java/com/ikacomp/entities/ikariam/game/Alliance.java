package com.ikacomp.entities.ikariam.game;

import com.ikacomp.entities.ikariam.board.ServerBoard;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

/**
 * Created by yoya on 2/14/15.
 */
@Entity
@Table(name = "alliance")
@Access(AccessType.PROPERTY)
@NamedQueries({
  @NamedQuery(name = "Alliance.findByAbbreviation", query = "SELECT a FROM Alliance a WHERE a.abbreviation = :abbreviation")
})
public class Alliance implements Serializable {

  private static final long serialVersionUID = -7250451131192942417L;

  private Integer allianceId;
  private String name;
  private String abbreviation;
  private ServerBoard serverBoard;
  private List<Player> playerList;

  public Alliance() {
  }

  public Alliance(String name) {
    this.name = name;
  }

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "ALLIANCE_ID")
  public Integer getAllianceId() {
    return allianceId;
  }

  public void setAllianceId(Integer allianceId) {
    this.allianceId = allianceId;
  }

  @Column(name = "NAME")
  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  @Column(name = "ABBREVIATION")
  public String getAbbreviation() {
    return abbreviation;
  }

  public void setAbbreviation(String abbreviation) {
    this.abbreviation = abbreviation;
  }

  @ManyToOne()
  @JoinColumn(name = "SERVER_BOARD_ID", referencedColumnName = "SERVER_BOARD_ID", updatable = false)
  public ServerBoard getServerBoard() {
    return serverBoard;
  }

  public void setServerBoard(ServerBoard serverBoard) {
    this.serverBoard = serverBoard;
  }

  @Transient
  public List<Player> getPlayerList() {
    return playerList;
  }

  public void setPlayerList(List<Player> playerList) {
    this.playerList = playerList;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;

    Alliance alliance = (Alliance) o;

    if (allianceId != null ? !allianceId.equals(alliance.allianceId) : alliance.allianceId != null)
      return false;
    return !(abbreviation != null ? !abbreviation.equals(alliance.abbreviation) : alliance.abbreviation != null);

  }

  @Override
  public int hashCode() {
    int result = allianceId != null ? allianceId.hashCode() : 0;
    result = 31 * result + (abbreviation != null ? abbreviation.hashCode() : 0);
    return result;
  }

  @Override
  public String toString() {
    return "Alliance{" +
      "allianceId=" + allianceId +
      ", name='" + name + '\'' +
      ", abbreviation='" + abbreviation + '\'' +
      ", serverBoard=" + serverBoard +
      ", playerList=" + playerList +
      '}';
  }
}
