package com.ikacomp.entities.ikariam.game;

import java.io.Serializable;

/**
 * Created by yoya on 2/14/15.
 */
public class Soldier implements Serializable {

	private static final long serialVersionUID = 167945263026023945L;

	private String name;
	private float generalPoints;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public float getGeneralPoints() {
		return generalPoints;
	}

	public void setGeneralPoints(float generalPoints) {
		this.generalPoints = generalPoints;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (!(o instanceof Soldier)) return false;

		Soldier soldier = (Soldier) o;

		if (Float.compare(soldier.generalPoints, generalPoints) != 0) return false;
		if (!name.equals(soldier.name)) return false;

		return true;
	}

	@Override
	public int hashCode() {
		int result = name.hashCode();
		result = 31 * result + (generalPoints != +0.0f ? Float.floatToIntBits(generalPoints) : 0);
		return result;
	}

	@Override
	public String toString() {
		return "Soldier{" +
				"name='" + name + '\'' +
				", generalPoints=" + generalPoints +
				'}';
	}
}
