package com.ikacomp.entities.ikariam.game;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

/**
 * Created by yoya on 2/14/15.
 */
@Entity
@Table(name = "world")
@Access(AccessType.PROPERTY)
public class World implements Serializable {

	private static final long serialVersionUID = 1717395307742483115L;

	private int worldId;
	private String name;
	private List<Server> servers;

	public World() {
	}
	public World(String name) {
		this.name = name;
	}

	@Id @GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "WORLD_ID")
	public int getWorldId() {
		return worldId;
	}

	public void setWorldId(int worldId) {
		this.worldId = worldId;
	}

	@Column(name = "NAME")
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@ManyToMany(fetch = FetchType.LAZY)
	@JoinTable(
			name = "world_server",
			joinColumns = {@JoinColumn(name = "WORLD_ID", referencedColumnName = "WORLD_ID")},
			inverseJoinColumns = {@JoinColumn(name = "SERVER_ID", referencedColumnName = "SERVER_ID")}
	)
	public List<Server> getServers() {
		return servers;
	}

	public void setServers(List<Server> servers) {
		this.servers = servers;
	}

	@Override
	public String toString() {
		return "World{" +
				"name='" + name + '\'' +
				", servers=" + servers +
				'}';
	}
}
