package com.ikacomp.entities.ikariam.game;

import java.io.Serializable;
import java.util.List;

/**
 * Created by yoya on 2/14/15.
 */
public class Army implements Serializable {

	private static final long serialVersionUID = 4396703604214810596L;

	private List<Soldier> soldiers;

	public List<Soldier> getSoldiers() {
		return soldiers;
	}

	public void setSoldiers(List<Soldier> soldiers) {
		this.soldiers = soldiers;
	}
}
